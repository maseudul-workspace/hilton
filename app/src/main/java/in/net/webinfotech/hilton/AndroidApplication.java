package in.net.webinfotech.hilton;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;

import in.net.webinfotech.hilton.domain.model.user.UserInfo;
import io.fabric.sdk.android.Fabric;

/**
 * Created by Raj on 21-01-2019.
 */

public class AndroidApplication extends Application {

    public String apiBaseURL;
    public UserInfo userInfo;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        apiBaseURL = getResources().getString(R.string.base_url);
    }

    public String getApiBaseURL() {
        return apiBaseURL;
    }

    public void setUserInfo(Context context, UserInfo userInfo){
        this.userInfo = userInfo;
        SharedPreferences sharedPref = context.getSharedPreferences(
                getString(R.string.KEY_HILTON_APP_PREFERENCES), Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPref.edit();

        if(userInfo != null) {
            editor.putString(getString(R.string.KEY_USER_DETAILS), new Gson().toJson(userInfo));
        } else {
            editor.putString(getString(R.string.KEY_USER_DETAILS), "");
        }

        editor.commit();
    }

    public UserInfo getUserInfo(Context context){
        UserInfo user;
        if(userInfo != null){
            user = userInfo;
        }else{
            SharedPreferences sharedPref = context.getSharedPreferences(
                    getString(R.string.KEY_HILTON_APP_PREFERENCES), Context.MODE_PRIVATE);
            String userInfoJson = sharedPref.getString(getString(R.string.KEY_USER_DETAILS),"");
            if(userInfoJson.isEmpty()){
                user = null;
            }else{
                try {
                    user = new Gson().fromJson(userInfoJson, UserInfo.class);
                    this.userInfo = user;
                }catch (Exception e){
                    user = null;
                }
            }
        }
        return user;
    }

}
