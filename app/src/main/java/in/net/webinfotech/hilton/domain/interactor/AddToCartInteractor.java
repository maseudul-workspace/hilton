package in.net.webinfotech.hilton.domain.interactor;

import in.net.webinfotech.hilton.domain.model.cart.AddToCartResponse;

/**
 * Created by Raj on 28-01-2019.
 */

public interface AddToCartInteractor {
    interface Callback{
        void onAddToCartSuccessful(AddToCartResponse response);
        void onAddToCartFail(String errorMsg);
    }
}
