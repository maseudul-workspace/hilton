package in.net.webinfotech.hilton.domain.interactor;

/**
 * Created by Raj on 01-02-2019.
 */

public interface CancelOrderInteractor {
    interface Callback{
        void onCancelOrderSuccess();
        void onCancelOrderFail(String errorMsg);
    }
}
