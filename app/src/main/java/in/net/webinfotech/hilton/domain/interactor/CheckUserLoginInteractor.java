package in.net.webinfotech.hilton.domain.interactor;

import in.net.webinfotech.hilton.domain.model.user.UserInfo;

/**
 * Created by Raj on 23-01-2019.
 */

public interface CheckUserLoginInteractor {
    interface Callback{
        void onLoginSuccess(UserInfo userInfo);
        void onLoginFail(String errorMsg);
    }
}
