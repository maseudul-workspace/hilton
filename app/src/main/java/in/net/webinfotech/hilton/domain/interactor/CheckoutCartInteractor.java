package in.net.webinfotech.hilton.domain.interactor;

/**
 * Created by Raj on 31-01-2019.
 */

public interface CheckoutCartInteractor {
    interface Callback{
        void onCheckoutSuccess();
        void onCheckoutFail(String errorMsg);
    }
}
