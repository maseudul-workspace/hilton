package in.net.webinfotech.hilton.domain.interactor;

/**
 * Created by Raj on 29-01-2019.
 */

public interface GetCartCountInteractor {
    interface Callback{
        void onGettingCartCountSuccess(int count);
        void onGettingCartCountFail(String errorMsg);
    }
}
