package in.net.webinfotech.hilton.domain.interactor;

import in.net.webinfotech.hilton.domain.model.cart.CartDetails;

/**
 * Created by Raj on 29-01-2019.
 */

public interface GetCartDetailsInteractor {
    interface Callback{
        void onGettingCartDetailsSuccess(CartDetails[] details);
        void onGettingCartDetalsFail(String errorMsg);
    }
}
