package in.net.webinfotech.hilton.domain.interactor;

/**
 * Created by Raj on 07-02-2019.
 */

public interface GetGreetingCountInteractor {
    interface Callback{
        void onGettingGreetingCountSuccess(Boolean greeting);
        void onGettingGreetingCountFail(String errorMsg);
    }
}
