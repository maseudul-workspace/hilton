package in.net.webinfotech.hilton.domain.interactor;

import in.net.webinfotech.hilton.domain.model.greeting.Greeting;

/**
 * Created by Raj on 06-02-2019.
 */

public interface GetGreetingsInteractor {
    interface Callback{
        void onGettingGreetingsSuccess(Greeting[] greetings);
        void onGettingGreetingsFail(String errorMsg);
    }
}
