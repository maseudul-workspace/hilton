package in.net.webinfotech.hilton.domain.interactor;

import in.net.webinfotech.hilton.domain.model.user.Ledger;

/**
 * Created by Raj on 25-02-2019.
 */

public interface GetLedgerTransactionsInteractor {
    interface Callback{
        void onGettingLedgerTransactionSuccess(Ledger ledger);
        void onGettingLedgerTransactionFail(String errorMsg);
    }
}
