package in.net.webinfotech.hilton.domain.interactor;

import in.net.webinfotech.hilton.domain.model.cart.Orders;

/**
 * Created by Raj on 01-02-2019.
 */

public interface GetOrderListInteractor {
    interface Callback{
        void onGettingOrderListSuccess(Orders[] orders, int totalPages);
        void onGettingOrderListFail(String errorMsg);
    }
}
