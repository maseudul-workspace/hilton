package in.net.webinfotech.hilton.domain.interactor;

import in.net.webinfotech.hilton.domain.model.product.Product;

/**
 * Created by Raj on 24-01-2019.
 */

public interface GetProductDetailsByIdInteractor {
    interface Callback{
        void onGettingProductDetailsSuccess(Product product);
        void onGettingProductDetailsFail(String errorMsg);
    }
}
