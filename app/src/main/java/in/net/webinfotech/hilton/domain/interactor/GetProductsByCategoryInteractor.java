package in.net.webinfotech.hilton.domain.interactor;

import in.net.webinfotech.hilton.domain.model.product.Product;

/**
 * Created by Raj on 22-01-2019.
 */

public interface GetProductsByCategoryInteractor {
    interface Callback{
        void onGettingProductListSuccess(Product[] products, int totalPage);
        void onGettingProductListFail(String errorMsg);
    }
}
