package in.net.webinfotech.hilton.domain.interactor;

import in.net.webinfotech.hilton.domain.model.product.Product;

/**
 * Created by Raj on 30-01-2019.
 */

public interface GetProductsByQueryInteractor {
    interface Callback{
        void onGettingProductListSuccess(Product[] products, String searchKey, int totalPage);
        void onGettingProductListFail(String errorMsg);
    }
}
