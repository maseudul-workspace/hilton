package in.net.webinfotech.hilton.domain.interactor;

import in.net.webinfotech.hilton.domain.model.user.UserCompleteDetails;

/**
 * Created by Raj on 28-01-2019.
 */

public interface GetUserDetailsInteractor {
    interface Callback{
        void onGettingUserDetailsSuccess(UserCompleteDetails userDetails);
        void onGettingUserDetailsFail(String errorMsg);
    }
}
