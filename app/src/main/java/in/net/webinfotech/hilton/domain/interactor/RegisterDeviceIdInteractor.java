package in.net.webinfotech.hilton.domain.interactor;

import in.net.webinfotech.hilton.domain.model.registration.DeviceIdResponse;

/**
 * Created by Raj on 21-01-2019.
 */

public interface RegisterDeviceIdInteractor {
    interface Callback{
        void onDeviceIdRegisterSuccess(DeviceIdResponse response);
        void onDeviceIdRegistrationFail(String errorMsg);
    }
}
