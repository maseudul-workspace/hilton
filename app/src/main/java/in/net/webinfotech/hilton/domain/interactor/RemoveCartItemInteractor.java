package in.net.webinfotech.hilton.domain.interactor;

/**
 * Created by Raj on 29-01-2019.
 */

public interface RemoveCartItemInteractor {
    interface Callback{
        void onItemRemovedSuccess();
        void onItemRemovedFail(String errorMsg);
    }
}
