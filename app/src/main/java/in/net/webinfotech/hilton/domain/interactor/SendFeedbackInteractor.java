package in.net.webinfotech.hilton.domain.interactor;

/**
 * Created by Raj on 06-02-2019.
 */

public interface SendFeedbackInteractor {
    interface Callback{
        void onSendingFeedbackSuccess(String successMsg);
        void onSendingFeedbackFail(String errorMsg);
    }
}
