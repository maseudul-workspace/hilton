package in.net.webinfotech.hilton.domain.interactor.impl;

import in.net.webinfotech.hilton.domain.executor.Executor;
import in.net.webinfotech.hilton.domain.executor.MainThread;
import in.net.webinfotech.hilton.domain.interactor.AddToCartInteractor;
import in.net.webinfotech.hilton.domain.interactor.base.AbstractInteractor;
import in.net.webinfotech.hilton.domain.model.cart.AddToCartResponse;
import in.net.webinfotech.hilton.repository.cart.CartRepositoryImpl;

/**
 * Created by Raj on 28-01-2019.
 */

public class AddToCartInteractorImpl extends AbstractInteractor implements AddToCartInteractor {

    String device_id;
    String api_key;
    int user_id;
    int product_id;
    int size_id;
    int quantity;
    Callback mCallback;
    CartRepositoryImpl mRepository;

    public AddToCartInteractorImpl(Executor threadExecutor,
                                   MainThread mainThread,
                                   Callback callback,
                                   CartRepositoryImpl repository,
                                   String device_id,
                                   String api_key,
                                   int user_id,
                                   int product_id,
                                   int size_id,
                                   int quantity) {
        super(threadExecutor, mainThread);
        this.mCallback = callback;
        this.mRepository = repository;
        this.device_id = device_id;
        this.user_id = user_id;
        this.api_key = api_key;
        this.product_id = product_id;
        this.size_id = size_id;
        this.quantity = quantity;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onAddToCartFail(errorMsg);
            }
        });
    }

    private void postMessage(final AddToCartResponse response){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onAddToCartSuccessful(response);
            }
        });
    }

    @Override
    public void run() {
        final AddToCartResponse response = mRepository.addToCart(device_id, api_key, user_id, product_id, size_id, quantity);
        if(response == null){
            notifyError("Something went wrong");
        }else if(!response.status && response.code == 400){
            notifyError(response.message);
        }else if(!response.status && response.code == 401){
            notifyError("GoToCart");
        }
        else{
            postMessage(response);
        }
    }
}
