package in.net.webinfotech.hilton.domain.interactor.impl;

import in.net.webinfotech.hilton.domain.executor.Executor;
import in.net.webinfotech.hilton.domain.executor.MainThread;
import in.net.webinfotech.hilton.domain.interactor.CancelOrderInteractor;
import in.net.webinfotech.hilton.domain.interactor.base.AbstractInteractor;
import in.net.webinfotech.hilton.domain.model.cart.CancelOrderResponse;
import in.net.webinfotech.hilton.repository.cart.CartRepositoryImpl;

/**
 * Created by Raj on 01-02-2019.
 */

public class CancelOrderInteractorImpl extends AbstractInteractor implements CancelOrderInteractor {

    Callback mCallback;
    CartRepositoryImpl mRepository;
    String deviceId;
    String apiKey;
    int userId;
    int orderDetailId;

    public CancelOrderInteractorImpl(Executor threadExecutor,
                                     MainThread mainThread,
                                     Callback callback,
                                     CartRepositoryImpl repository,
                                     String deviceId,
                                     String apiKey,
                                     int userId,
                                     int orderDetailId) {
        super(threadExecutor, mainThread);
        this.mCallback = callback;
        this.mRepository = repository;
        this.deviceId = deviceId;
        this.apiKey = apiKey;
        this.userId = userId;
        this.orderDetailId = orderDetailId;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onCancelOrderFail(errorMsg);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onCancelOrderSuccess();
            }
        });
    }

    @Override
    public void run() {
        final CancelOrderResponse cancelOrderResponse = mRepository.cancelOrder(deviceId, apiKey, userId, orderDetailId);
        if(cancelOrderResponse == null){
            notifyError("Something went wrong");
        }else if(!cancelOrderResponse.status){
            notifyError(cancelOrderResponse.message);
        }else{
            postMessage();
        }
    }
}
