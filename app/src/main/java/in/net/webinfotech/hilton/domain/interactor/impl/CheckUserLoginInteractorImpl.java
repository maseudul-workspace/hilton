package in.net.webinfotech.hilton.domain.interactor.impl;

import in.net.webinfotech.hilton.domain.executor.Executor;
import in.net.webinfotech.hilton.domain.executor.MainThread;
import in.net.webinfotech.hilton.domain.interactor.CheckUserLoginInteractor;
import in.net.webinfotech.hilton.domain.interactor.base.AbstractInteractor;
import in.net.webinfotech.hilton.domain.model.user.UserInfo;
import in.net.webinfotech.hilton.domain.model.user.UserInfoWrapper;
import in.net.webinfotech.hilton.repository.user.UserRepositoryImpl;

/**
 * Created by Raj on 23-01-2019.
 */

public class CheckUserLoginInteractorImpl extends AbstractInteractor implements CheckUserLoginInteractor {

    UserRepositoryImpl mRepsository;
    String email;
    String password;
    String deviceId;
    Callback mCallback;

    public CheckUserLoginInteractorImpl(Executor threadExecutor,
                                        MainThread mainThread,
                                        Callback callback,
                                        UserRepositoryImpl repsository,
                                        String email,
                                        String password,
                                        String deviceId) {
        super(threadExecutor, mainThread);
        this.email = email;
        this.password = password;
        this.deviceId = deviceId;
        this.mCallback = callback;
        this.mRepsository = repsository;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onLoginFail(errorMsg);
            }
        });
    }

    private void postMessage(final UserInfo userInfo){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onLoginSuccess(userInfo);
            }
        });
    }

    @Override
    public void run() {
        final UserInfoWrapper userInfoWrapper = mRepsository.checkLogin(email, password, deviceId);
        if(userInfoWrapper == null){
            notifyError("Login unsuccessful !!! Something went wrong");
        }else if (userInfoWrapper != null && userInfoWrapper.status == false){
            notifyError(userInfoWrapper.message);
        }else{
            postMessage(userInfoWrapper.userInfo);
        }
    }
}
