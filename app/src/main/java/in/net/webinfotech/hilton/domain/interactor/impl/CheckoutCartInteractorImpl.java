package in.net.webinfotech.hilton.domain.interactor.impl;

import in.net.webinfotech.hilton.domain.executor.Executor;
import in.net.webinfotech.hilton.domain.executor.MainThread;
import in.net.webinfotech.hilton.domain.interactor.CheckoutCartInteractor;
import in.net.webinfotech.hilton.domain.interactor.base.AbstractInteractor;
import in.net.webinfotech.hilton.domain.model.cart.CheckoutCartResponse;
import in.net.webinfotech.hilton.repository.cart.CartRepositoryImpl;

/**
 * Created by Raj on 31-01-2019.
 */

public class CheckoutCartInteractorImpl extends AbstractInteractor implements CheckoutCartInteractor {

    Callback mCallback;
    CartRepositoryImpl mRepository;
    String deviceId;
    String apiKey;
    int userId;
    int paymentMethod;

    public CheckoutCartInteractorImpl(Executor threadExecutor,
                                      MainThread mainThread,
                                      Callback callback,
                                      CartRepositoryImpl repository,
                                      String deviceId,
                                      String apiKey,
                                      int userId,
                                      int paymentMethod
                                      ) {
        super(threadExecutor, mainThread);
        this.mCallback = callback;
        this.mRepository = repository;
        this.deviceId = deviceId;
        this.apiKey = apiKey;
        this.userId = userId;
        this.paymentMethod = paymentMethod;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onCheckoutFail(errorMsg);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onCheckoutSuccess();
            }
        });
    }

    @Override
    public void run() {
        CheckoutCartResponse response = mRepository.checkoutCart(deviceId, apiKey, userId, paymentMethod);
        if(response == null){
            notifyError("Something went wrong");
        }else if(!response.status){
            notifyError(response.message);
        }else{
            postMessage();
        }
    }
}
