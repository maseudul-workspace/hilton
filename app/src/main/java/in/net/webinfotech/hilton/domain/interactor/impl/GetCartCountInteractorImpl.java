package in.net.webinfotech.hilton.domain.interactor.impl;

import in.net.webinfotech.hilton.domain.executor.Executor;
import in.net.webinfotech.hilton.domain.executor.MainThread;
import in.net.webinfotech.hilton.domain.interactor.GetCartCountInteractor;
import in.net.webinfotech.hilton.domain.interactor.GetCartDetailsInteractor;
import in.net.webinfotech.hilton.domain.interactor.base.AbstractInteractor;
import in.net.webinfotech.hilton.domain.model.cart.CartDetails;
import in.net.webinfotech.hilton.domain.model.cart.CartDetailsWrapper;
import in.net.webinfotech.hilton.repository.cart.CartRepositoryImpl;

/**
 * Created by Raj on 29-01-2019.
 */

public class GetCartCountInteractorImpl extends AbstractInteractor implements GetCartCountInteractor {


    Callback mCallback;
    CartRepositoryImpl mRepository;
    String devceId;
    String apiKey;
    int userId;

    public GetCartCountInteractorImpl(Executor threadExecutor, MainThread mainThread,
                                      Callback callback,
                                      CartRepositoryImpl repository,
                                      String devceId,
                                      String apiKey,
                                      int userId) {
        super(threadExecutor, mainThread);
        this.mCallback = callback;
        this.mRepository = repository;
        this.devceId = devceId;
        this.apiKey = apiKey;
        this.userId = userId;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingCartCountFail(errorMsg);
            }
        });
    }

    private void postMessage(final int count){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingCartCountSuccess(count);
            }
        });
    }


    @Override
    public void run() {
        final CartDetailsWrapper detailsWrapper = mRepository.getCartDetails(devceId, apiKey, userId);
        if(detailsWrapper == null){
            notifyError("Something went wrong");
        }else if(!detailsWrapper.status && detailsWrapper.cartDetails.length == 0){
            notifyError("");
        }else{
            postMessage(detailsWrapper.cartDetails.length);
        }
    }
}
