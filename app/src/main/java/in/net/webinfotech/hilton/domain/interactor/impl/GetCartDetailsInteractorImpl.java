package in.net.webinfotech.hilton.domain.interactor.impl;

import in.net.webinfotech.hilton.domain.executor.Executor;
import in.net.webinfotech.hilton.domain.executor.MainThread;
import in.net.webinfotech.hilton.domain.interactor.GetCartDetailsInteractor;
import in.net.webinfotech.hilton.domain.interactor.base.AbstractInteractor;
import in.net.webinfotech.hilton.domain.model.cart.CartDetails;
import in.net.webinfotech.hilton.domain.model.cart.CartDetailsWrapper;
import in.net.webinfotech.hilton.domain.model.product.Product;
import in.net.webinfotech.hilton.repository.cart.CartRepositoryImpl;

/**
 * Created by Raj on 29-01-2019.
 */

public class GetCartDetailsInteractorImpl extends AbstractInteractor implements GetCartDetailsInteractor {

    Callback mCallback;
    CartRepositoryImpl mRepository;
    String devceId;
    String apiKey;
    int userId;

    public GetCartDetailsInteractorImpl(Executor threadExecutor,
                                        MainThread mainThread,
                                        Callback callback,
                                        CartRepositoryImpl repository,
                                        String devceId,
                                        String apiKey,
                                        int userId) {
        super(threadExecutor, mainThread);
        this.mCallback = callback;
        this.mRepository = repository;
        this.devceId = devceId;
        this.apiKey = apiKey;
        this.userId = userId;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingCartDetalsFail(errorMsg);
            }
        });
    }

    private void postMessage(final CartDetails[] cartDetails){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingCartDetailsSuccess(cartDetails);
            }
        });
    }


    @Override
    public void run() {
        CartDetailsWrapper detailsWrapper = mRepository.getCartDetails(devceId, apiKey, userId);
        if(detailsWrapper == null){
            notifyError("Something went wrong");
        }else if(!detailsWrapper.status){
            notifyError(detailsWrapper.message);
        }else{
            postMessage(detailsWrapper.cartDetails);
        }
    }
}
