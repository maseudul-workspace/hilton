package in.net.webinfotech.hilton.domain.interactor.impl;

import in.net.webinfotech.hilton.domain.executor.Executor;
import in.net.webinfotech.hilton.domain.executor.MainThread;
import in.net.webinfotech.hilton.domain.interactor.GetGreetingCountInteractor;
import in.net.webinfotech.hilton.domain.interactor.GetGreetingsInteractor;
import in.net.webinfotech.hilton.domain.interactor.base.AbstractInteractor;
import in.net.webinfotech.hilton.domain.model.greeting.GreetingsCount;
import in.net.webinfotech.hilton.repository.greetings.GreetingsRepositoryImpl;

/**
 * Created by Raj on 07-02-2019.
 */

public class GetGreetingCountInteractorImpl extends AbstractInteractor implements GetGreetingCountInteractor {

    Callback mCallback;
    String deviceId;
    int userId;
    String apikey;
    GreetingsRepositoryImpl mRepository;

    public GetGreetingCountInteractorImpl(Executor threadExecutor,
                                          MainThread mainThread,
                                          Callback callback,
                                          GreetingsRepositoryImpl repository,
                                          String deviceId,
                                          int userId,
                                          String apikey) {
        super(threadExecutor, mainThread);
        this.mCallback = callback;
        this.deviceId = deviceId;
        this.userId = userId;
        this.apikey = apikey;
        this.mRepository = repository;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingGreetingCountFail(errorMsg);
            }
        });
    }

    private void postMessage(final Boolean greeting){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingGreetingCountSuccess(greeting);
            }
        });
    }

    @Override
    public void run() {
        final GreetingsCount greetingsCount = mRepository.getGreetingCount(apikey, userId, deviceId);
        if(greetingsCount == null){
            notifyError("Something went wrong");
        }else if(!greetingsCount.status){
            notifyError(greetingsCount.message);
        }else {
            postMessage(greetingsCount.greeting);
        }
    }
}
