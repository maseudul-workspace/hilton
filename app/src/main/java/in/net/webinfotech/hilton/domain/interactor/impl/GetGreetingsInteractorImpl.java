package in.net.webinfotech.hilton.domain.interactor.impl;

import in.net.webinfotech.hilton.domain.executor.Executor;
import in.net.webinfotech.hilton.domain.executor.MainThread;
import in.net.webinfotech.hilton.domain.interactor.GetGreetingsInteractor;
import in.net.webinfotech.hilton.domain.interactor.base.AbstractInteractor;
import in.net.webinfotech.hilton.domain.model.greeting.Greeting;
import in.net.webinfotech.hilton.domain.model.greeting.GreetingsWrapper;
import in.net.webinfotech.hilton.repository.greetings.GreetingsRepositoryImpl;

/**
 * Created by Raj on 06-02-2019.
 */

public class GetGreetingsInteractorImpl extends AbstractInteractor implements GetGreetingsInteractor {

    Callback mCallback;
    String deviceId;
    int userId;
    String apikey;
    GreetingsRepositoryImpl mRepository;

    public GetGreetingsInteractorImpl(Executor threadExecutor,
                                      MainThread mainThread,
                                      Callback callback,
                                      GreetingsRepositoryImpl repository,
                                      String deviceId,
                                      int userId,
                                      String apikey
                                      ) {
        super(threadExecutor, mainThread);
        this.mCallback = callback;
        this.deviceId = deviceId;
        this.userId = userId;
        this.apikey = apikey;
        this.mRepository = repository;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingGreetingsFail(errorMsg);
            }
        });
    }

    private void postMessage(final Greeting[] greetings){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingGreetingsSuccess(greetings);
            }
        });
    }

    @Override
    public void run() {
        final GreetingsWrapper greetingsWrapper = mRepository.getGreetingsList(apikey, userId, deviceId);
        if(greetingsWrapper == null){
            notifyError("Something went wrong");
        }else if(!greetingsWrapper.status){
            notifyError(greetingsWrapper.message);
        }else {
            postMessage(greetingsWrapper.greetings);
        }
    }
}
