package in.net.webinfotech.hilton.domain.interactor.impl;

import in.net.webinfotech.hilton.domain.executor.Executor;
import in.net.webinfotech.hilton.domain.executor.MainThread;
import in.net.webinfotech.hilton.domain.interactor.GetLedgerTransactionsInteractor;
import in.net.webinfotech.hilton.domain.interactor.base.AbstractInteractor;
import in.net.webinfotech.hilton.domain.model.user.Ledger;
import in.net.webinfotech.hilton.domain.model.user.LedgerWrapper;
import in.net.webinfotech.hilton.repository.user.UserRepositoryImpl;

/**
 * Created by Raj on 25-02-2019.
 */

public class GetLedgerTransactionInteractorImpl extends AbstractInteractor implements GetLedgerTransactionsInteractor {
    Callback mCallback;
    String deviceiD;
    String apiKey;
    int userId;
    UserRepositoryImpl mRepository;
    public GetLedgerTransactionInteractorImpl(Executor threadExecutor,
                                              MainThread mainThread,
                                              UserRepositoryImpl repository,
                                              Callback callback,
                                              String deviceiD,
                                              String apiKey,
                                              int userId
                                              ) {
        super(threadExecutor, mainThread);
        this.mCallback = callback;
        this.deviceiD = deviceiD;
        this.apiKey = apiKey;
        this.userId = userId;
        this.mRepository = repository;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingLedgerTransactionFail(errorMsg);
            }
        });
    }

    private void postMessage(final Ledger ledger){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingLedgerTransactionSuccess(ledger);
            }
        });
    }

    @Override
    public void run() {
        final LedgerWrapper ledgerWrapper = mRepository.getLedgerTransactions(apiKey, userId, deviceiD);
        if(ledgerWrapper == null){
            notifyError("Something went wrong");
        }else if(!ledgerWrapper.status){
            notifyError(ledgerWrapper.message);
        }else{
            postMessage(ledgerWrapper.ledger);
        }
    }
}
