package in.net.webinfotech.hilton.domain.interactor.impl;

import in.net.webinfotech.hilton.domain.executor.Executor;
import in.net.webinfotech.hilton.domain.executor.MainThread;
import in.net.webinfotech.hilton.domain.interactor.GetOrderListInteractor;
import in.net.webinfotech.hilton.domain.interactor.base.AbstractInteractor;
import in.net.webinfotech.hilton.domain.model.cart.Orders;
import in.net.webinfotech.hilton.domain.model.cart.OrdersWrapper;
import in.net.webinfotech.hilton.repository.cart.CartRepositoryImpl;

/**
 * Created by Raj on 01-02-2019.
 */

public class GetOrderListInteractorImpl extends AbstractInteractor implements GetOrderListInteractor {

    Callback mCallback;
    CartRepositoryImpl mRepository;
    String devceId;
    String apiKey;
    int userId;
    int pageNo;

    public GetOrderListInteractorImpl(Executor threadExecutor,
                                      MainThread mainThread,
                                      Callback callback,
                                      CartRepositoryImpl repository,
                                      String devceId,
                                      String apiKey,
                                      int userId,
                                      int pageNo) {
        super(threadExecutor, mainThread);
        this.mCallback = callback;
        this.mRepository = repository;
        this.devceId = devceId;
        this.apiKey = apiKey;
        this.userId = userId;
        this.pageNo = pageNo;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingOrderListFail(errorMsg);
            }
        });
    }

    private void postMessage(final Orders[] orders, final int totalpages){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingOrderListSuccess(orders, totalpages);
            }
        });
    }


    @Override
    public void run() {
        final OrdersWrapper ordersWrapper = mRepository.getOrders(devceId, apiKey,userId, pageNo);
        if(ordersWrapper == null){
            notifyError("Something went wrong");
        }else if(!ordersWrapper.status){
            notifyError(ordersWrapper.message);
        }else{
            postMessage(ordersWrapper.orders, ordersWrapper.total_page);
        }
    }
}
