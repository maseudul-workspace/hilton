package in.net.webinfotech.hilton.domain.interactor.impl;

import in.net.webinfotech.hilton.domain.executor.Executor;
import in.net.webinfotech.hilton.domain.executor.MainThread;
import in.net.webinfotech.hilton.domain.interactor.GetProductDetailsByIdInteractor;
import in.net.webinfotech.hilton.domain.interactor.GetProductsByCategoryInteractor;
import in.net.webinfotech.hilton.domain.interactor.base.AbstractInteractor;
import in.net.webinfotech.hilton.domain.model.product.Product;
import in.net.webinfotech.hilton.domain.model.product.ProductDetailsWrapper;
import in.net.webinfotech.hilton.domain.model.user.UserInfo;
import in.net.webinfotech.hilton.repository.products.ProductRepositoryImpl;

/**
 * Created by Raj on 24-01-2019.
 */

public class GetProductDetailsByIdInteractorImpl extends AbstractInteractor implements GetProductDetailsByIdInteractor{

    ProductRepositoryImpl mRepository;
    String deviceId;
    int productId;
    Callback mCallback;

    public GetProductDetailsByIdInteractorImpl(Executor threadExecutor,
                                               MainThread mainThread,
                                               ProductRepositoryImpl repository,
                                               String deviceId,
                                               int productId,
                                               Callback callback) {
        super(threadExecutor, mainThread);
        this.deviceId = deviceId;
        this.productId = productId;
        this.mCallback = callback;
        this.mRepository = repository;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingProductDetailsFail(errorMsg);
            }
        });
    }

    private void postMessage(final Product product){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingProductDetailsSuccess(product);
            }
        });
    }

    @Override
    public void run() {
        final ProductDetailsWrapper productDetailsWrapper = mRepository.getProductDetailsById(productId, deviceId);
        if(productDetailsWrapper == null){
            notifyError("Something went wrong");
        }else if(productDetailsWrapper != null && productDetailsWrapper.status == false){
            notifyError(productDetailsWrapper.message);
        }else{
            postMessage(productDetailsWrapper.products);
        }
    }
}
