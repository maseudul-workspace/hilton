package in.net.webinfotech.hilton.domain.interactor.impl;

import in.net.webinfotech.hilton.domain.executor.Executor;
import in.net.webinfotech.hilton.domain.executor.MainThread;
import in.net.webinfotech.hilton.domain.interactor.GetProductsByCategoryInteractor;
import in.net.webinfotech.hilton.domain.interactor.base.AbstractInteractor;
import in.net.webinfotech.hilton.domain.model.product.Product;
import in.net.webinfotech.hilton.domain.model.product.ProductListWrapper;
import in.net.webinfotech.hilton.repository.products.ProductRepositoryImpl;

/**
 * Created by Raj on 22-01-2019.
 */

public class GetProductsByCategoryInteractorImpl extends AbstractInteractor implements GetProductsByCategoryInteractor {

    ProductRepositoryImpl mRepository;
    Callback mCallback;
    int category_id;
    int page_no;
    String device_id;

    public GetProductsByCategoryInteractorImpl(Executor threadExecutor,
                                               MainThread mainThread,
                                               Callback callback,
                                               ProductRepositoryImpl repository,
                                               int category_id,
                                               int page_no,
                                               String device_id) {
        super(threadExecutor, mainThread);
        this.mCallback = callback;
        this.category_id = category_id;
        this.page_no = page_no;
        this.device_id = device_id;
        this.mRepository = repository;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingProductListFail(errorMsg);
            }
        });
    }

    private void postMessage(final Product[] products, final int totalPage){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingProductListSuccess(products, totalPage);
            }
        });
    }

    @Override
    public void run() {
        final ProductListWrapper products = mRepository.getProductByCategory(category_id, page_no, device_id);
        if(products == null){
            notifyError("Something went wrong");
        }else if(products != null && products.status == false){
            notifyError("No products available for this category");
        }
        else{
            postMessage(products.products, products.total_page);
        }

    }
}
