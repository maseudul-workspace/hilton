package in.net.webinfotech.hilton.domain.interactor.impl;

import in.net.webinfotech.hilton.domain.executor.Executor;
import in.net.webinfotech.hilton.domain.executor.MainThread;
import in.net.webinfotech.hilton.domain.interactor.GetProductsByQueryInteractor;
import in.net.webinfotech.hilton.domain.interactor.base.AbstractInteractor;
import in.net.webinfotech.hilton.domain.model.product.Product;
import in.net.webinfotech.hilton.domain.model.product.ProductListWrapper;
import in.net.webinfotech.hilton.repository.products.ProductRepositoryImpl;

/**
 * Created by Raj on 30-01-2019.
 */

public class GetProductsByQueryInteractorImpl extends AbstractInteractor implements GetProductsByQueryInteractor {

    Callback mCallback;
    ProductRepositoryImpl mRepository;
    String deviceId;
    String searchKey;
    int pageNo;

    public GetProductsByQueryInteractorImpl(Executor threadExecutor,
                                            MainThread mainThread,
                                            Callback callback,
                                            ProductRepositoryImpl repository,
                                            String deviceId,
                                            String searchKey,
                                            int pageNo) {
        super(threadExecutor, mainThread);
        this.deviceId = deviceId;
        this.searchKey = searchKey;
        this.pageNo = pageNo;
        this.mCallback = callback;
        this.mRepository = repository;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingProductListFail(errorMsg);
            }
        });
    }

    private void postMessage(final Product[] products, final String searchKey, final int totalPage){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingProductListSuccess(products, searchKey, totalPage);
            }
        });
    }

    @Override
    public void run() {
        final ProductListWrapper products = mRepository.getProductsByQuery(deviceId, searchKey, pageNo);
        if(products == null){
            notifyError("Something went wrong");
        }else if(products.products.length == 0 && products.status == false){
            postMessage(products.products, products.search_key, products.total_page);
        }
        else{
            postMessage(products.products, products.search_key, products.total_page);
        }
    }
}
