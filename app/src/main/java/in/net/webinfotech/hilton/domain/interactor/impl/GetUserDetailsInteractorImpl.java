package in.net.webinfotech.hilton.domain.interactor.impl;

import in.net.webinfotech.hilton.domain.executor.Executor;
import in.net.webinfotech.hilton.domain.executor.MainThread;
import in.net.webinfotech.hilton.domain.interactor.GetUserDetailsInteractor;
import in.net.webinfotech.hilton.domain.interactor.base.AbstractInteractor;
import in.net.webinfotech.hilton.domain.model.user.UserCompleteDetails;
import in.net.webinfotech.hilton.domain.model.user.UserCompleteDetailsWrapper;
import in.net.webinfotech.hilton.domain.model.user.UserInfo;
import in.net.webinfotech.hilton.repository.user.UserRepositoryImpl;

/**
 * Created by Raj on 28-01-2019.
 */

public class GetUserDetailsInteractorImpl extends AbstractInteractor implements GetUserDetailsInteractor {

    Callback mCallback;
    String deviceiD;
    String apiKey;
    int userId;
    UserRepositoryImpl mRepository;

    public GetUserDetailsInteractorImpl(Executor threadExecutor,
                                        MainThread mainThread,
                                        UserRepositoryImpl repository,
                                        Callback callback,
                                        String deviceiD,
                                        String apiKey,
                                        int userId
                                        ) {
        super(threadExecutor, mainThread);
        this.mCallback = callback;
        this.deviceiD = deviceiD;
        this.apiKey = apiKey;
        this.userId = userId;
        this.mRepository = repository;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingUserDetailsFail(errorMsg);
            }
        });
    }

    private void postMessage(final UserCompleteDetails userDetails){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingUserDetailsSuccess(userDetails);
            }
        });
    }

    @Override
    public void run() {
        final UserCompleteDetailsWrapper userDetailsWrapper = this.mRepository.getUserDetails(apiKey, userId, deviceiD);
        if(userDetailsWrapper == null){
            notifyError("Something went wrong");
        }else if(!userDetailsWrapper.status){
            notifyError(userDetailsWrapper.message);
        }else{
            postMessage(userDetailsWrapper.userDetails);
        }
    }
}
