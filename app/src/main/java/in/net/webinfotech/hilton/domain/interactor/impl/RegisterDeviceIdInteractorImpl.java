package in.net.webinfotech.hilton.domain.interactor.impl;

import in.net.webinfotech.hilton.domain.executor.Executor;
import in.net.webinfotech.hilton.domain.executor.MainThread;
import in.net.webinfotech.hilton.domain.interactor.RegisterDeviceIdInteractor;
import in.net.webinfotech.hilton.domain.interactor.base.AbstractInteractor;
import in.net.webinfotech.hilton.domain.model.registration.DeviceIdResponse;
import in.net.webinfotech.hilton.repository.registration.RegistrationRepositoryImpl;

/**
 * Created by Raj on 21-01-2019.
 */

public class RegisterDeviceIdInteractorImpl extends AbstractInteractor implements RegisterDeviceIdInteractor {

    Callback mCallback;
    String deviceId;
    RegistrationRepositoryImpl mRepository;


    public RegisterDeviceIdInteractorImpl(Executor threadExecutor,
                                          MainThread mainThread,
                                          Callback callback,
                                          RegistrationRepositoryImpl repository,
                                          String deviceId) {
        super(threadExecutor, mainThread);
        this.mCallback = callback;
        this.mRepository = repository;
        this.deviceId = deviceId;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onDeviceIdRegistrationFail(errorMsg);
            }
        });
    }

    private void postMessage(final DeviceIdResponse response){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onDeviceIdRegisterSuccess(response);
            }
        });
    }

    @Override
    public void run() {
        final DeviceIdResponse response = mRepository.setDeviceId(deviceId);
        if(response == null){
            notifyError("");
        }else{
            postMessage(response);
        }
    }
}
