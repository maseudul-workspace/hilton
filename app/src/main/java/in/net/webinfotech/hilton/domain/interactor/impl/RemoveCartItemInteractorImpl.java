package in.net.webinfotech.hilton.domain.interactor.impl;

import in.net.webinfotech.hilton.domain.executor.Executor;
import in.net.webinfotech.hilton.domain.executor.MainThread;
import in.net.webinfotech.hilton.domain.interactor.RemoveCartItemInteractor;
import in.net.webinfotech.hilton.domain.interactor.base.AbstractInteractor;
import in.net.webinfotech.hilton.domain.model.cart.RemoveCartItemResponse;
import in.net.webinfotech.hilton.repository.cart.CartRepositoryImpl;

/**
 * Created by Raj on 29-01-2019.
 */

public class RemoveCartItemInteractorImpl extends AbstractInteractor implements RemoveCartItemInteractor {

    Callback mCallback;
    CartRepositoryImpl mRepository;
    String decviceId;
    String apiKey;
    int userId;
    int cartId;

    public RemoveCartItemInteractorImpl(Executor threadExecutor,
                                        MainThread mainThread,
                                        Callback callback,
                                        CartRepositoryImpl repository,
                                        String deviceId,
                                        String apiKey,
                                        int userId,
                                        int cartId) {
        super(threadExecutor, mainThread);
        this.mCallback = callback;
        this.mRepository = repository;
        this.decviceId = deviceId;
        this.apiKey = apiKey;
        this.userId = userId;
        this.cartId = cartId;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onItemRemovedFail(errorMsg);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onItemRemovedSuccess();
            }
        });
    }

    @Override
    public void run() {
        RemoveCartItemResponse response = mRepository.removeCartItem(decviceId, apiKey, userId, cartId);
        if(response == null){
            notifyError("Something went wrong");
        }else if(!response.status){
            notifyError(response.message);
        }else{
            postMessage();
        }
    }
}
