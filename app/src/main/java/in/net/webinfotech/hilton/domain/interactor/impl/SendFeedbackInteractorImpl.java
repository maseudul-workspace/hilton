package in.net.webinfotech.hilton.domain.interactor.impl;

import in.net.webinfotech.hilton.domain.executor.Executor;
import in.net.webinfotech.hilton.domain.executor.MainThread;
import in.net.webinfotech.hilton.domain.interactor.SendFeedbackInteractor;
import in.net.webinfotech.hilton.domain.interactor.base.AbstractInteractor;
import in.net.webinfotech.hilton.domain.model.user.Feedback;
import in.net.webinfotech.hilton.repository.user.UserRepositoryImpl;

/**
 * Created by Raj on 06-02-2019.
 */

public class SendFeedbackInteractorImpl extends AbstractInteractor implements SendFeedbackInteractor {

    Callback mCallback;
    String deviceiD;
    String apiKey;
    int userId;
    String feedbackMsg;
    UserRepositoryImpl mRepository;

    public SendFeedbackInteractorImpl(Executor threadExecutor,
                                      MainThread mainThread,
                                      UserRepositoryImpl repository,
                                      Callback callback,
                                      String deviceiD,
                                      String apiKey,
                                      int userId,
                                      String feeedbackMsg
                                      ) {
        super(threadExecutor, mainThread);
        this.mCallback = callback;
        this.deviceiD = deviceiD;
        this.apiKey = apiKey;
        this.userId = userId;
        this.mRepository = repository;
        this.feedbackMsg = feeedbackMsg;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onSendingFeedbackFail(errorMsg);
            }
        });
    }

    private void postMessage(final String successMsg){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onSendingFeedbackSuccess(successMsg);
            }
        });
    }


    @Override
    public void run() {
        Feedback feedback = mRepository.giveFeedback(apiKey, userId, deviceiD, feedbackMsg);
        if(feedback == null){
            notifyError("Something went wrong");
        }else if(!feedback.status){
            notifyError(feedback.message);
        }else{
            postMessage(feedback.message);
        }
    }
}
