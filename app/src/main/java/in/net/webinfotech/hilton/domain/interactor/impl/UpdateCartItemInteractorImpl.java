package in.net.webinfotech.hilton.domain.interactor.impl;

import in.net.webinfotech.hilton.domain.executor.Executor;
import in.net.webinfotech.hilton.domain.executor.MainThread;
import in.net.webinfotech.hilton.domain.interactor.UpdateCartItemInteractor;
import in.net.webinfotech.hilton.domain.interactor.base.AbstractInteractor;
import in.net.webinfotech.hilton.domain.model.cart.UpdateCartResponse;
import in.net.webinfotech.hilton.repository.cart.CartRepositoryImpl;

/**
 * Created by Raj on 29-01-2019.
 */

public class UpdateCartItemInteractorImpl extends AbstractInteractor implements UpdateCartItemInteractor {

    Callback mCallback;
    CartRepositoryImpl mRepository;
    String deviceId;
    String apiKey;
    int userId;
    int cartId;
    int quantiity;

    public UpdateCartItemInteractorImpl(Executor threadExecutor,
                                        MainThread mainThread,
                                        Callback callback,
                                        CartRepositoryImpl repository,
                                        String deviceId,
                                        String apiKey,
                                        int userId,
                                        int cartId,
                                        int quantity) {
        super(threadExecutor, mainThread);
        this.mCallback = callback;
        this.mRepository = repository;
        this.deviceId = deviceId;
        this.apiKey = apiKey;
        this.userId = userId;
        this.cartId = cartId;
        this.quantiity = quantity;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onCartItemUpdateFail(errorMsg);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onCartItemUpdateSuccess();
            }
        });
    }

    @Override
    public void run() {
        UpdateCartResponse response = mRepository.updateCartItem(deviceId, apiKey, userId, cartId, quantiity);
        if(response == null){
            notifyError("Something went wrong");
        }else if(!response.status){
            notifyError(response.message);
        }else{
            postMessage();
        }
    }
}
