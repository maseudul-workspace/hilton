package in.net.webinfotech.hilton.domain.model.cart;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import in.net.webinfotech.hilton.domain.model.product.Product;

/**
 * Created by Raj on 29-01-2019.
 */

public class CartDetails {
    @SerializedName("cart_id")
    @Expose
    public int cart_id;

    @SerializedName("user_id")
    @Expose
    public int user_id;

    @SerializedName("product_id")
    @Expose
    public int size_id;

    @SerializedName("quantity")
    @Expose
    public int quantity;

    @SerializedName("date")
    @Expose
    public String date;

    @SerializedName("product")
    @Expose
    public Product product;

}
