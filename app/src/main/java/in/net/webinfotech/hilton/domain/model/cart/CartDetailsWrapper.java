package in.net.webinfotech.hilton.domain.model.cart;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import in.net.webinfotech.hilton.domain.model.product.Product;

/**
 * Created by Raj on 29-01-2019.
 */

public class CartDetailsWrapper {
    @SerializedName("status")
    @Expose
    public boolean status;

    @SerializedName("message")
    @Expose
    public String message;

    @SerializedName("code")
    @Expose
    public int code;

    @SerializedName("data")
    @Expose
    public CartDetails[] cartDetails;
}
