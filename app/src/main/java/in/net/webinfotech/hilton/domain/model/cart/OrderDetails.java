package in.net.webinfotech.hilton.domain.model.cart;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 31-01-2019.
 */

public class OrderDetails {

    @SerializedName("order_detail_id")
    @Expose
    public int order_detail_id;

    @SerializedName("product_id")
    @Expose
    public int product_id;

    @SerializedName("product_name")
    @Expose
    public String product_name;

    @SerializedName("product_image")
    @Expose
    public String product_image;

    @SerializedName("product_size")
    @Expose
    public String product_size;

    @SerializedName("order_quantity")
    @Expose
    public int order_quantity;

    @SerializedName("order_status")
    @Expose
    public int order_status;

    @SerializedName("offer_type")
    @Expose
    public int offer_type;

    @SerializedName("offer")
    @Expose
    public Integer offer;



}
