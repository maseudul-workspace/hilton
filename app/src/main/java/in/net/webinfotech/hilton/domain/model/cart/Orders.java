package in.net.webinfotech.hilton.domain.model.cart;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 31-01-2019.
 */

public class Orders {
    @SerializedName("order_id")
    @Expose
    public int order_id;

    @SerializedName("user_id")
    @Expose
    public int user_id;

    @SerializedName("date")
    @Expose
    public String date;

    @SerializedName("products")
    @Expose
    public OrderDetails[] orderDetails;

}
