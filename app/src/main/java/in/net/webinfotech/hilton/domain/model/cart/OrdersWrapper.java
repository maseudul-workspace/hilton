package in.net.webinfotech.hilton.domain.model.cart;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 01-02-2019.
 */

public class OrdersWrapper {
    @SerializedName("status")
    @Expose
    public boolean status;

    @SerializedName("message")
    @Expose
    public String message;

    @SerializedName("search_key")
    @Expose
    public String search_key;

    @SerializedName("code")
    @Expose
    public int code;

    @SerializedName("total_page")
    @Expose
    public int total_page;

    @SerializedName("data")
    @Expose
    public Orders[] orders;

}
