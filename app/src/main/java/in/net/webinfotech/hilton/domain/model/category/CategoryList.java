package in.net.webinfotech.hilton.domain.model.category;

/**
 * Created by Raj on 18-01-2019.
 */

public class CategoryList {
    public String categoryName;
    public int drawableId;
    public int categoryid;

    public CategoryList(String categoryName, int drawableId, int categoryid) {
        this.categoryName = categoryName;
        this.drawableId = drawableId;
        this.categoryid = categoryid;
    }
}
