package in.net.webinfotech.hilton.domain.model.greeting;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 07-02-2019.
 */

public class Greeting {

    @SerializedName("greeting_id")
    @Expose
    public int greetingId;

    @SerializedName("description")
    @Expose
    public String description;

    @SerializedName("date")
    @Expose
    public String date;

}
