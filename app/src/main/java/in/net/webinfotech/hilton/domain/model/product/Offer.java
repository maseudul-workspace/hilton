package in.net.webinfotech.hilton.domain.model.product;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 21-01-2019.
 */

public class Offer {
    @SerializedName("offer_id")
    @Expose
    public int offer_id;

    @SerializedName("buy")
    @Expose
    public int buy;

    @SerializedName("offer")
    @Expose
    public int offer;

    @SerializedName("offer_type")
    @Expose
    public int offer_type;

    @SerializedName("dates")
    @Expose
    public String dates;

}
