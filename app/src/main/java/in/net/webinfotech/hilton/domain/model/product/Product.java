package in.net.webinfotech.hilton.domain.model.product;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 22-01-2019.
 */

public class Product {
    @SerializedName("product_id")
    @Expose
    public int product_id;

    @SerializedName("product_name")
    @Expose
    public String product_name;

    @SerializedName("product_composition")
    @Expose
    public String product_composition;

    @SerializedName("description")
    @Expose
    public String description;

    @SerializedName("image")
    @Expose
    public String image;

    @SerializedName("status")
    @Expose
    public int status;

    @SerializedName("category_name")
    @Expose
    public String category_name;

    @SerializedName("category_id")
    @Expose
    public int category_id;

    @SerializedName("dates")
    @Expose
    public String dates;

    @SerializedName("size")
    @Expose
    public Size[] sizes;

}
