package in.net.webinfotech.hilton.domain.model.product;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 22-01-2019.
 */

public class ProductListWrapper {
    @SerializedName("status")
    @Expose
    public boolean status;

    @SerializedName("message")
    @Expose
    public String message;

    @SerializedName("search_key")
    @Expose
    public String search_key;

    @SerializedName("code")
    @Expose
    public int code;

    @SerializedName("total_page")
    @Expose
    public int total_page;

    @SerializedName("data")
    @Expose
    public Product[] products;


}
