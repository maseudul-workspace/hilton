package in.net.webinfotech.hilton.domain.model.product;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 21-01-2019.
 */

public class Size {
    @SerializedName("size_id")
    @Expose
    public int size_id;

    @SerializedName("product_size")
    @Expose
    public String product_size;

    @SerializedName("size_name")
    @Expose
    public String size_name;

    @SerializedName("packing_size")
    @Expose
    public int packing_size;

    @SerializedName("packing_name")
    @Expose
    public String packing_name;

    @SerializedName("price")
    @Expose
    public double price;

    @SerializedName("stock")
    @Expose
    public int stock;

    @SerializedName("offer")
    @Expose
    public Offer[] offers;

}
