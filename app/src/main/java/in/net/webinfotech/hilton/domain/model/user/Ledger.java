package in.net.webinfotech.hilton.domain.model.user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 25-02-2019.
 */

public class Ledger {
    @SerializedName("user_id")
    @Expose
    public int user_id;

    @SerializedName("outstanding")
    @Expose
    public Long outstanding;

    @SerializedName("details")
    @Expose
    public LedgerTransaction[] ledgerTransactions;

}
