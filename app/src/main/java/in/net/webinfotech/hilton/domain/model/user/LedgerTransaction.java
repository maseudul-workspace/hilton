package in.net.webinfotech.hilton.domain.model.user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 25-02-2019.
 */

public class LedgerTransaction {
    @SerializedName("purchase_amount")
    @Expose
    public Long purchase_amount;

    @SerializedName("purchase_date")
    @Expose
    public String purchase_date;

    @SerializedName("receive_amount")
    @Expose
    public Long receive_amount;

    @SerializedName("receive_date")
    @Expose
    public String receive_date;

    @SerializedName("status")
    @Expose
    public int status;

    @SerializedName("comment")
    @Expose
    public String comment;

}
