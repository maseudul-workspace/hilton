package in.net.webinfotech.hilton.domain.model.user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 23-01-2019.
 */

public class UserInfo {
    @SerializedName("user_id")
    @Expose
    public int user_id;

    @SerializedName("api_key")
    @Expose
    public String api_key;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("email")
    @Expose
    public String email;

    @SerializedName("mobile")
    @Expose
    public long mobile;
}
