package in.net.webinfotech.hilton.presentation.presenters;

import in.net.webinfotech.hilton.presentation.presenters.base.BasePresenter;

/**
 * Created by Raj on 29-01-2019.
 */

public interface BaseActivityPresenter extends BasePresenter {
    void getCartCount(String deviceId, String apiKey, int userId);
    void getGreetingCount(String deviceId, String apiKey, int userId);
    interface View{
        void loadCartCount(int count);
        void loadGreetingCount(Boolean greeting);
    }
}
