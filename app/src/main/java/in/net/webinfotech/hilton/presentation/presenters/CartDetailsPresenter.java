package in.net.webinfotech.hilton.presentation.presenters;

import in.net.webinfotech.hilton.presentation.presenters.base.BasePresenter;
import in.net.webinfotech.hilton.presentation.ui.adapters.RecyclerViewCartItemList;

/**
 * Created by Raj on 29-01-2019.
 */

public interface CartDetailsPresenter extends BasePresenter {
    void getCartDetails(String deviceId, String apiKey, int userId);
    void checkout(String deviceId, String apiKey, int userId, int paymentMethod);
    interface View{
        void loadRecyclerViewAdapter(RecyclerViewCartItemList adapter);
        void showProgressBar();
        void hideProgressBar();
        void showOrderSnackbar();
    }
}
