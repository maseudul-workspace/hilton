package in.net.webinfotech.hilton.presentation.presenters;

import in.net.webinfotech.hilton.presentation.presenters.base.BasePresenter;

/**
 * Created by Raj on 06-02-2019.
 */

public interface FeedbackPresenter extends BasePresenter{
    interface View{
        void showProgressBar();
        void onFeedbackSuccess();
        void hideProgressBar();
    }
    void sendFeedback(String deviceId, String apiKey, int userId, String feedbackMsg);
}
