package in.net.webinfotech.hilton.presentation.presenters;

import in.net.webinfotech.hilton.presentation.presenters.base.BasePresenter;
import in.net.webinfotech.hilton.presentation.ui.adapters.RecyclerViewGreetings;

/**
 * Created by Raj on 06-02-2019.
 */

public interface GreetingsPresenter extends BasePresenter {
    void getGreetings(String deviceId, String apiKey, int userId);
    interface View{
        void loadRecyclerViewGreetings(RecyclerViewGreetings adapter);
        void showProgressbar();
        void hideProgressbar();
    }
}
