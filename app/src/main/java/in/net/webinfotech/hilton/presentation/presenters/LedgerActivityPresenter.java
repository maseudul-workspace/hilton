package in.net.webinfotech.hilton.presentation.presenters;

import in.net.webinfotech.hilton.presentation.presenters.base.BasePresenter;
import in.net.webinfotech.hilton.presentation.ui.adapters.RecyclerViewLedgerTransactions;

/**
 * Created by Raj on 25-02-2019.
 */

public interface LedgerActivityPresenter extends BasePresenter{
    void getLedgerTransactions(String apiKey, int userId, String deviceId);
    interface View{
        void showProgressBar();
        void hideProgressbar();
        void loadAdapter(RecyclerViewLedgerTransactions adapter, Long outstanding);
    }
}
