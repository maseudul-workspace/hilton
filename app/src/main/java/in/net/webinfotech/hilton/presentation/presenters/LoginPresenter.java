package in.net.webinfotech.hilton.presentation.presenters;

import in.net.webinfotech.hilton.presentation.presenters.base.BasePresenter;

/**
 * Created by Raj on 23-01-2019.
 */

public interface LoginPresenter extends BasePresenter {
    void checkLogin(String email, String password, String deviceId);
    interface View{
        void hideProgressBar();
        void showProgressBar();
    }
}
