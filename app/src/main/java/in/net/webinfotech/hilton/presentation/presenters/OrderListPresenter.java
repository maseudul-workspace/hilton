package in.net.webinfotech.hilton.presentation.presenters;

import in.net.webinfotech.hilton.presentation.presenters.base.BasePresenter;
import in.net.webinfotech.hilton.presentation.ui.adapters.RecyclerViewOrderIds;

/**
 * Created by Raj on 01-02-2019.
 */

public interface OrderListPresenter extends BasePresenter {
    void getOrderList(String deviceId, String apiKey, int userId, int pageNo, String type);
    interface View{
        void loadOrdersAdapter(RecyclerViewOrderIds adapter, int totalPageCount);
        void showProgressBar();
        void hideProgressBar();
        void hidePaginationProgressBar();
        void showPaginationProgressBar();
        void stopRefreshing();
        void onOrderDeleteSuccess();
    }
}
