package in.net.webinfotech.hilton.presentation.presenters;

import in.net.webinfotech.hilton.domain.model.product.Product;
import in.net.webinfotech.hilton.presentation.presenters.base.BasePresenter;

/**
 * Created by Raj on 24-01-2019.
 */

public interface ProductDetailsPresenter extends BasePresenter {
    interface View{
        void setProductDetails(Product productDetails);
        void showGoToCartSnackbar();
        void showProgressBar();
        void hideProgressBar();
    }
    void getProductDetails(int productId, String deviceId);
    void addToCart(String device_id, String api_key, int user_id, int product_id, int size_id, int quantity);
}
