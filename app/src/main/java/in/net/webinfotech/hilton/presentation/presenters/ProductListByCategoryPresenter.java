package in.net.webinfotech.hilton.presentation.presenters;

import in.net.webinfotech.hilton.presentation.presenters.base.BasePresenter;
import in.net.webinfotech.hilton.presentation.ui.adapters.RecyclerViewProductList;

/**
 * Created by Raj on 22-01-2019.
 */

public interface ProductListByCategoryPresenter extends BasePresenter {
    interface View{
        void loadRecyclerViewAdapter(RecyclerViewProductList adapter, int totalPage);
        void showProgressBar();
        void hideProgressBar();
        void hidePaginationProgressBar();
        void showPaginationProgressBar();
        void stopRefreshing();
    }
    void getProductList(int category_id, int page_no, String device_id, String type);
}
