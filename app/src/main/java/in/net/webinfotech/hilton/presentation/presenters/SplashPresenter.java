package in.net.webinfotech.hilton.presentation.presenters;

import in.net.webinfotech.hilton.presentation.presenters.base.BasePresenter;

/**
 * Created by Raj on 21-01-2019.
 */

public interface SplashPresenter extends BasePresenter {
    void sendDeviceId(String deviceId);
}
