package in.net.webinfotech.hilton.presentation.presenters;

import in.net.webinfotech.hilton.domain.model.user.UserCompleteDetails;
import in.net.webinfotech.hilton.presentation.presenters.base.BasePresenter;

/**
 * Created by Raj on 28-01-2019.
 */

public interface UserDetailsPresenter extends BasePresenter {
    void getUserDetails(String apiKey, int userId, String deviceId);
    interface View{
        void loadUserDetails(UserCompleteDetails userDetails);
        void showProgressBar();
        void hideProgressBar();
    }
}
