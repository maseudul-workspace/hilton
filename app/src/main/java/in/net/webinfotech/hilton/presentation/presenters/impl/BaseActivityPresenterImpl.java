package in.net.webinfotech.hilton.presentation.presenters.impl;

import android.content.Context;
import android.util.Log;

import in.net.webinfotech.hilton.domain.executor.Executor;
import in.net.webinfotech.hilton.domain.executor.MainThread;
import in.net.webinfotech.hilton.domain.interactor.GetCartCountInteractor;
import in.net.webinfotech.hilton.domain.interactor.GetGreetingCountInteractor;
import in.net.webinfotech.hilton.domain.interactor.impl.GetCartCountInteractorImpl;
import in.net.webinfotech.hilton.domain.interactor.impl.GetCartDetailsInteractorImpl;
import in.net.webinfotech.hilton.domain.interactor.impl.GetGreetingCountInteractorImpl;
import in.net.webinfotech.hilton.presentation.presenters.BaseActivityPresenter;
import in.net.webinfotech.hilton.presentation.presenters.base.AbstractPresenter;
import in.net.webinfotech.hilton.repository.cart.CartRepositoryImpl;
import in.net.webinfotech.hilton.repository.greetings.GreetingsRepositoryImpl;

/**
 * Created by Raj on 29-01-2019.
 */

public class BaseActivityPresenterImpl extends AbstractPresenter implements BaseActivityPresenter, GetCartCountInteractor.Callback,
                                                                                                    GetGreetingCountInteractor.Callback{

    Context mContext;
    GetCartCountInteractorImpl mInteractor;
    BaseActivityPresenter.View mView;
    GetGreetingCountInteractorImpl greetingCountInteractor;

    public BaseActivityPresenterImpl(Executor executor,
                                     MainThread mainThread,
                                     Context context,
                                     BaseActivityPresenter.View view) {
        super(executor, mainThread);
        this.mContext = context;
        this.mView = view;
    }

    @Override
    public void getCartCount(String deviceId, String apiKey, int userId) {
        mInteractor = new GetCartCountInteractorImpl(mExecutor,
                mMainThread,
                this,
                new CartRepositoryImpl(),
                deviceId, apiKey, userId);
        mInteractor.execute();
    }

    @Override
    public void onGettingCartCountSuccess(int count) {
        mView.loadCartCount(count);
    }

    @Override
    public void onGettingCartCountFail(String errorMsg) {
    }

    @Override
    public void getGreetingCount(String deviceId, String apiKey, int userId) {
        greetingCountInteractor = new GetGreetingCountInteractorImpl(mExecutor, mMainThread,
                                                                    this, new GreetingsRepositoryImpl(),
                                                                    deviceId, userId, apiKey);
        greetingCountInteractor.execute();
    }

    @Override
    public void onGettingGreetingCountSuccess(Boolean greeting) {
        mView.loadGreetingCount(greeting);
    }

    @Override
    public void onGettingGreetingCountFail(String errorMsg) {
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }
}
