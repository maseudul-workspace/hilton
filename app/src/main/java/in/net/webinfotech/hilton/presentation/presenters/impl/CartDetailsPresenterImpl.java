package in.net.webinfotech.hilton.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import in.net.webinfotech.hilton.domain.executor.Executor;
import in.net.webinfotech.hilton.domain.executor.MainThread;
import in.net.webinfotech.hilton.domain.interactor.CheckoutCartInteractor;
import in.net.webinfotech.hilton.domain.interactor.GetCartCountInteractor;
import in.net.webinfotech.hilton.domain.interactor.GetCartDetailsInteractor;
import in.net.webinfotech.hilton.domain.interactor.RemoveCartItemInteractor;
import in.net.webinfotech.hilton.domain.interactor.UpdateCartItemInteractor;
import in.net.webinfotech.hilton.domain.interactor.impl.CheckoutCartInteractorImpl;
import in.net.webinfotech.hilton.domain.interactor.impl.GetCartDetailsInteractorImpl;
import in.net.webinfotech.hilton.domain.interactor.impl.RemoveCartItemInteractorImpl;
import in.net.webinfotech.hilton.domain.interactor.impl.UpdateCartItemInteractorImpl;
import in.net.webinfotech.hilton.domain.model.cart.CartDetails;
import in.net.webinfotech.hilton.presentation.presenters.CartDetailsPresenter;
import in.net.webinfotech.hilton.presentation.presenters.base.AbstractPresenter;
import in.net.webinfotech.hilton.presentation.routers.CartDetailsRouter;
import in.net.webinfotech.hilton.presentation.ui.adapters.RecyclerViewCartItemList;
import in.net.webinfotech.hilton.repository.cart.CartRepositoryImpl;

/**
 * Created by Raj on 29-01-2019.
 */

public class CartDetailsPresenterImpl extends AbstractPresenter implements CartDetailsPresenter,
                                                                            GetCartDetailsInteractor.Callback,
                                                                            RemoveCartItemInteractor.Callback,
                                                                            RecyclerViewCartItemList.Callback,
                                                                            UpdateCartItemInteractor.Callback,
                                                                            CheckoutCartInteractor.Callback{

    Context mContext;
    GetCartDetailsInteractorImpl mInteractor;
    CartDetailsPresenter.View mView;
    CartDetails[] cartDetails = null;
    RecyclerViewCartItemList adapter;
    RemoveCartItemInteractorImpl removeCartItemInteractor;
    UpdateCartItemInteractorImpl updateCartItemInteractor;
    CheckoutCartInteractorImpl checkoutCartInteractor;
    CartDetailsRouter mRouter;
    String deviceId;
    String apiKey;
    int userId;

    public CartDetailsPresenterImpl(Executor executor,
                                    MainThread mainThread,
                                    Context context,
                                    CartDetailsPresenter.View view,
                                    CartDetailsRouter router) {
        super(executor, mainThread);
        this.mContext = context;
        this.mView = view;
        this.mRouter = router;
    }


    @Override
    public void getCartDetails(String deviceId, String apiKey, int userId) {
        this.deviceId = deviceId;
        this.apiKey = apiKey;
        this.userId = userId;
        mInteractor = new GetCartDetailsInteractorImpl(mExecutor,
                                                        mMainThread,
                                                        this,
                                                        new CartRepositoryImpl(),
                                                        deviceId, apiKey, userId);
        mInteractor.execute();
    }

    @Override
    public void onGettingCartDetailsSuccess(CartDetails[] details) {
        if(cartDetails == null){
            cartDetails = details;
            adapter = new RecyclerViewCartItemList(mContext, cartDetails, this);
            mView.loadRecyclerViewAdapter(adapter);
        }else{
            cartDetails = details;
            adapter.updateCartItem(details);
            adapter.notifyDataSetChanged();
        }
        mView.hideProgressBar();
    }

    @Override
    public void onGettingCartDetalsFail(String errorMsg) {
        mView.hideProgressBar();
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRemoveButtonClicked(int cartId) {
        mView.showProgressBar();
        removeCartItemInteractor = new RemoveCartItemInteractorImpl(mExecutor,
                                                                    mMainThread,
                                                                    this,
                                                                    new CartRepositoryImpl(),
                                                                    deviceId, apiKey,
                                                                    userId, cartId);
        removeCartItemInteractor.execute();
    }

    @Override
    public void onItemRemovedSuccess() {
        mInteractor = new GetCartDetailsInteractorImpl(mExecutor,
                mMainThread,
                this,
                new CartRepositoryImpl(),
                deviceId, apiKey, userId);
        mInteractor.execute();
    }

    @Override
    public void onItemRemovedFail(String errorMsg) {
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
        mView.hideProgressBar();
    }

    @Override
    public void onUpdateButtonClicked(int cartId, int quantity) {
        mView.showProgressBar();
        updateCartItemInteractor = new UpdateCartItemInteractorImpl(mExecutor,
                mMainThread, this,  new CartRepositoryImpl(), deviceId, apiKey, userId, cartId, quantity);
        updateCartItemInteractor.execute();
    }

    @Override
    public void onCartItemUpdateSuccess() {
        mInteractor = new GetCartDetailsInteractorImpl(mExecutor,
                mMainThread,
                this,
                new CartRepositoryImpl(),
                deviceId, apiKey, userId);
        mInteractor.execute();
        Toast.makeText(mContext, "Item successfully updated", Toast.LENGTH_LONG);
    }

    @Override
    public void onCartItemUpdateFail(String errorMsg) {
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
        mView.hideProgressBar();
    }

    @Override
    public void goToProductDetails(int productId) {
        mRouter.goToProductDetails(productId);
    }

    @Override
    public void checkout(String deviceId, String apiKey, int userId, int paymentMethod) {
        checkoutCartInteractor = new CheckoutCartInteractorImpl(mExecutor,
                                                                mMainThread, this,
                                                                new CartRepositoryImpl(), deviceId,
                                                                apiKey, userId, paymentMethod);
        checkoutCartInteractor.execute();
    }

    @Override
    public void onCheckoutSuccess() {
        mInteractor = new GetCartDetailsInteractorImpl(mExecutor,
                mMainThread,
                this,
                new CartRepositoryImpl(),
                deviceId, apiKey, userId);
        mInteractor.execute();
        mView.showOrderSnackbar();
    }

    @Override
    public void onCheckoutFail(String errorMsg) {
        mView.hideProgressBar();
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }
}
