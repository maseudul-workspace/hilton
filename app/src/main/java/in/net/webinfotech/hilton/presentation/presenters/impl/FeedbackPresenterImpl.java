package in.net.webinfotech.hilton.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import in.net.webinfotech.hilton.domain.executor.Executor;
import in.net.webinfotech.hilton.domain.executor.MainThread;
import in.net.webinfotech.hilton.domain.interactor.SendFeedbackInteractor;
import in.net.webinfotech.hilton.domain.interactor.impl.SendFeedbackInteractorImpl;
import in.net.webinfotech.hilton.presentation.presenters.FeedbackPresenter;
import in.net.webinfotech.hilton.presentation.presenters.base.AbstractPresenter;
import in.net.webinfotech.hilton.repository.user.UserRepositoryImpl;

/**
 * Created by Raj on 06-02-2019.
 */

public class FeedbackPresenterImpl extends AbstractPresenter implements FeedbackPresenter, SendFeedbackInteractor.Callback {

    Context mContext;
    SendFeedbackInteractorImpl mInteractor;
    FeedbackPresenter.View mView;

    public FeedbackPresenterImpl(Executor executor,
                                 MainThread mainThread,
                                 Context context,
                                 FeedbackPresenter.View view
                                 ) {
        super(executor, mainThread);
        this.mContext = context;
        this.mView = view;
    }

    @Override
    public void sendFeedback(String deviceId, String apiKey, int userId, String feedbackMsg) {
        mInteractor = new SendFeedbackInteractorImpl(mExecutor, mMainThread,
                                                    new UserRepositoryImpl(), this,
                                                    deviceId, apiKey,
                                                    userId, feedbackMsg);
        mInteractor.execute();
    }

    @Override
    public void onSendingFeedbackSuccess(String successMsg) {
        mView.hideProgressBar();
        mView.onFeedbackSuccess();
        Toast.makeText(mContext, successMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSendingFeedbackFail(String errorMsg) {
        mView.hideProgressBar();
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }
}
