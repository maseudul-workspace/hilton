package in.net.webinfotech.hilton.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import in.net.webinfotech.hilton.domain.executor.Executor;
import in.net.webinfotech.hilton.domain.executor.MainThread;
import in.net.webinfotech.hilton.domain.interactor.GetGreetingsInteractor;
import in.net.webinfotech.hilton.domain.interactor.impl.GetGreetingsInteractorImpl;
import in.net.webinfotech.hilton.domain.model.greeting.Greeting;
import in.net.webinfotech.hilton.presentation.presenters.GreetingsPresenter;
import in.net.webinfotech.hilton.presentation.presenters.base.AbstractPresenter;
import in.net.webinfotech.hilton.presentation.ui.adapters.RecyclerViewGreetings;
import in.net.webinfotech.hilton.repository.greetings.GreetingsRepositoryImpl;

/**
 * Created by Raj on 06-02-2019.
 */

public class GreetingsPresenterImpl extends AbstractPresenter implements GreetingsPresenter, GetGreetingsInteractor.Callback {

    Context mContext;
    GreetingsPresenter.View mView;
    GetGreetingsInteractorImpl mInteractor;
    RecyclerViewGreetings recyclerViewGreetings;

    public GreetingsPresenterImpl(Executor executor, MainThread mainThread, Context context, GreetingsPresenter.View view) {
        super(executor, mainThread);
        this.mContext = context;
        this.mView = view;
    }

    @Override
    public void getGreetings(String deviceId, String apiKey, int userId) {
        mInteractor = new GetGreetingsInteractorImpl(mExecutor, mMainThread,
                                                    this, new GreetingsRepositoryImpl(),
                                                    deviceId, userId, apiKey);
        mInteractor.execute();
    }

    @Override
    public void onGettingGreetingsSuccess(Greeting[] greetings) {
        recyclerViewGreetings = new RecyclerViewGreetings(mContext, greetings);
        mView.loadRecyclerViewGreetings(recyclerViewGreetings);
        mView.hideProgressbar();
    }

    @Override
    public void onGettingGreetingsFail(String errorMsg) {
        mView.hideProgressbar();
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }
}
