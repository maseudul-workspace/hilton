package in.net.webinfotech.hilton.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import in.net.webinfotech.hilton.domain.executor.Executor;
import in.net.webinfotech.hilton.domain.executor.MainThread;
import in.net.webinfotech.hilton.domain.interactor.GetLedgerTransactionsInteractor;
import in.net.webinfotech.hilton.domain.interactor.impl.GetLedgerTransactionInteractorImpl;
import in.net.webinfotech.hilton.domain.model.user.Ledger;
import in.net.webinfotech.hilton.presentation.presenters.LedgerActivityPresenter;
import in.net.webinfotech.hilton.presentation.presenters.base.AbstractPresenter;
import in.net.webinfotech.hilton.presentation.ui.adapters.RecyclerViewLedgerTransactions;
import in.net.webinfotech.hilton.repository.user.UserRepositoryImpl;

/**
 * Created by Raj on 25-02-2019.
 */

public class LedgerActivityPresenterImpl extends AbstractPresenter implements LedgerActivityPresenter,
                                                                            GetLedgerTransactionsInteractor.Callback {

    Context mContext;
    LedgerActivityPresenter.View mView;
    GetLedgerTransactionInteractorImpl mInteractor;
    RecyclerViewLedgerTransactions adapter;

    public LedgerActivityPresenterImpl(Executor executor,
                                       MainThread mainThread,
                                       Context context,
                                       LedgerActivityPresenter.View view
                                       ) {
        super(executor, mainThread);
        mContext = context;
        mView = view;
    }

    @Override
    public void getLedgerTransactions(String apiKey, int userId, String deviceId) {
        mInteractor = new GetLedgerTransactionInteractorImpl(mExecutor, mMainThread,
                                                            new UserRepositoryImpl(), this,
                                                            deviceId, apiKey, userId);
        mInteractor.execute();
    }

    @Override
    public void onGettingLedgerTransactionSuccess(Ledger ledger) {
        adapter = new RecyclerViewLedgerTransactions(mContext, ledger.ledgerTransactions);
        mView.loadAdapter(adapter, ledger.outstanding);
        mView.hideProgressbar();
    }

    @Override
    public void onGettingLedgerTransactionFail(String errorMsg) {
        mView.hideProgressbar();
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }
}
