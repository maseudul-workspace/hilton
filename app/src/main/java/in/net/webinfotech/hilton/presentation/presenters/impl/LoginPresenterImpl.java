package in.net.webinfotech.hilton.presentation.presenters.impl;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import in.net.webinfotech.hilton.AndroidApplication;
import in.net.webinfotech.hilton.domain.executor.Executor;
import in.net.webinfotech.hilton.domain.executor.MainThread;
import in.net.webinfotech.hilton.domain.interactor.CheckUserLoginInteractor;
import in.net.webinfotech.hilton.domain.interactor.impl.CheckUserLoginInteractorImpl;
import in.net.webinfotech.hilton.domain.model.user.UserInfo;
import in.net.webinfotech.hilton.presentation.presenters.LoginPresenter;
import in.net.webinfotech.hilton.presentation.presenters.base.AbstractPresenter;
import in.net.webinfotech.hilton.presentation.routers.LoginRouter;
import in.net.webinfotech.hilton.repository.user.UserRepositoryImpl;

/**
 * Created by Raj on 23-01-2019.
 */

public class LoginPresenterImpl extends AbstractPresenter implements LoginPresenter,CheckUserLoginInteractor.Callback {


    Context mContext;
    CheckUserLoginInteractorImpl mInteractor;
    AndroidApplication androidApplication;
    LoginRouter mRouter;
    LoginPresenter.View mView;

    public LoginPresenterImpl(Executor executor,
                              MainThread mainThread,
                              Context context,
                              LoginRouter router,
                              LoginPresenter.View view) {
        super(executor, mainThread);
        this.mContext = context;
        this.mRouter = router;
        this.mView = view;
    }

    @Override
    public void checkLogin(String email, String password, String deviceId) {
        mInteractor = new CheckUserLoginInteractorImpl(mExecutor, mMainThread,
                                                    this,
                                                    new UserRepositoryImpl(),
                                                    email,
                                                    password,
                                                    deviceId);
        mInteractor.execute();
    }

    @Override
    public void onLoginSuccess(UserInfo userInfo) {
        Toast.makeText(mContext, "Login successful",Toast.LENGTH_SHORT).show();
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        androidApplication.setUserInfo(mContext, userInfo);
        mView.hideProgressBar();
        mRouter.goToMain();
    }

    @Override
    public void onLoginFail(String errorMsg) {
        mView.hideProgressBar();
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }
}
