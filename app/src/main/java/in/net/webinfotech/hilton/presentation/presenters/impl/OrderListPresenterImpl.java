package in.net.webinfotech.hilton.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import in.net.webinfotech.hilton.domain.executor.Executor;
import in.net.webinfotech.hilton.domain.executor.MainThread;
import in.net.webinfotech.hilton.domain.interactor.CancelOrderInteractor;
import in.net.webinfotech.hilton.domain.interactor.GetOrderListInteractor;
import in.net.webinfotech.hilton.domain.interactor.impl.CancelOrderInteractorImpl;
import in.net.webinfotech.hilton.domain.interactor.impl.GetOrderListInteractorImpl;
import in.net.webinfotech.hilton.domain.model.cart.Orders;
import in.net.webinfotech.hilton.presentation.presenters.base.AbstractPresenter;
import in.net.webinfotech.hilton.presentation.ui.adapters.RecyclerViewOrderIds;
import in.net.webinfotech.hilton.presentation.ui.adapters.RecyclerViewOrderProducts;
import in.net.webinfotech.hilton.repository.cart.CartRepositoryImpl;

/**
 * Created by Raj on 01-02-2019.
 */

public class OrderListPresenterImpl extends AbstractPresenter implements in.net.webinfotech.hilton.presentation.presenters.OrderListPresenter,
                                                                        GetOrderListInteractor.Callback,
                                                                        RecyclerViewOrderProducts.Callback,
                                                                        CancelOrderInteractor.Callback{

    GetOrderListInteractorImpl mInteractor;
    in.net.webinfotech.hilton.presentation.presenters.OrderListPresenter.View mView;
    Context mContext;
    RecyclerViewOrderIds adapter;
    CancelOrderInteractorImpl cancelOrderInteractor;
    Orders[] newOrders;
    String deviceId;
    String apiKey;
    int userId;


    public OrderListPresenterImpl(Executor executor,
                                  MainThread mainThread,
                                  Context context,
                                  in.net.webinfotech.hilton.presentation.presenters.OrderListPresenter.View view) {
        super(executor, mainThread);
        this.mContext = context;
        this.mView = view;
    }

    @Override
    public void getOrderList(String deviceId, String apiKey, int userId, int pageNo, String type) {
        this.deviceId = deviceId;
        this.apiKey = apiKey;
        this.userId = userId;
        if(type.contains("refresh")){
            newOrders = null;
        }
        mInteractor = new GetOrderListInteractorImpl(mExecutor, mMainThread, this, new CartRepositoryImpl(), deviceId, apiKey, userId, pageNo);
        mInteractor.execute();
    }

    @Override
    public void onGettingOrderListSuccess(Orders[] orders, int totalPage) {
        Orders[] tempOrders;
        tempOrders = newOrders;
        try {
            int len1 = tempOrders.length;
            int len2 = orders.length;
            newOrders = new Orders[len1 + len2];
            System.arraycopy(tempOrders, 0, newOrders, 0, len1);
            System.arraycopy(orders, 0, newOrders, len1, len2);
            adapter.addItems(newOrders);
            adapter.notifyDataSetChanged();
            mView.hidePaginationProgressBar();
        }catch (NullPointerException e){
            newOrders = orders;
            adapter = new RecyclerViewOrderIds(orders, mContext, this);
            mView.loadOrdersAdapter(adapter, totalPage);
            mView.hideProgressBar();
        }
        mView.stopRefreshing();
    }

    @Override
    public void onGettingOrderListFail(String errorMsg) {
        mView.hideProgressBar();
        mView.hidePaginationProgressBar();
        mView.stopRefreshing();
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onCancelBtnClicked(int orderDetailId) {
        mView.showProgressBar();
        cancelOrderInteractor = new CancelOrderInteractorImpl(mExecutor, mMainThread,
                                                            this, new CartRepositoryImpl(),
                                                                this.deviceId, this.apiKey, userId, orderDetailId);
        cancelOrderInteractor.execute();
    }

    @Override
    public void onCancelOrderSuccess() {
        mView.onOrderDeleteSuccess();
    }

    @Override
    public void onCancelOrderFail(String errorMsg) {
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
        mView.hideProgressBar();
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }
}
