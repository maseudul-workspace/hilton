package in.net.webinfotech.hilton.presentation.presenters.impl;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import in.net.webinfotech.hilton.domain.executor.Executor;
import in.net.webinfotech.hilton.domain.executor.MainThread;
import in.net.webinfotech.hilton.domain.interactor.AddToCartInteractor;
import in.net.webinfotech.hilton.domain.interactor.GetProductDetailsByIdInteractor;
import in.net.webinfotech.hilton.domain.interactor.impl.AddToCartInteractorImpl;
import in.net.webinfotech.hilton.domain.interactor.impl.GetProductDetailsByIdInteractorImpl;
import in.net.webinfotech.hilton.domain.interactor.impl.GetProductsByCategoryInteractorImpl;
import in.net.webinfotech.hilton.domain.model.cart.AddToCartResponse;
import in.net.webinfotech.hilton.domain.model.product.Product;
import in.net.webinfotech.hilton.presentation.presenters.ProductDetailsPresenter;
import in.net.webinfotech.hilton.presentation.presenters.base.AbstractPresenter;
import in.net.webinfotech.hilton.repository.cart.CartRepositoryImpl;
import in.net.webinfotech.hilton.repository.products.ProductRepositoryImpl;

/**
 * Created by Raj on 24-01-2019.
 */

public class ProductDetailsPresenterImpl extends AbstractPresenter implements ProductDetailsPresenter,
                                                                                GetProductDetailsByIdInteractor.Callback,
                                                                                AddToCartInteractor.Callback{

    Context mContext;
    ProductDetailsPresenter.View mView;
    GetProductDetailsByIdInteractorImpl mInteractor;
    AddToCartInteractorImpl cartInteractor;

    public ProductDetailsPresenterImpl(Executor executor,
                                       MainThread mainThread,
                                       Context context,
                                       ProductDetailsPresenter.View view) {
        super(executor, mainThread);
        this.mView = view;
        this.mContext = context;
    }

    @Override
    public void getProductDetails(int productId, String deviceId) {
        mInteractor = new GetProductDetailsByIdInteractorImpl(mExecutor,
                                                                mMainThread,
                                                                new ProductRepositoryImpl(),
                                                                deviceId,
                                                                productId,
                                                                this);
        mInteractor.execute();
    }

    @Override
    public void onGettingProductDetailsSuccess(Product product) {
        mView.setProductDetails(product);
        mView.hideProgressBar();
    }

    @Override
    public void onGettingProductDetailsFail(String errorMsg) {
        mView.hideProgressBar();
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void addToCart(String device_id, String api_key, int user_id, int product_id, int size_id, int quantity) {
        cartInteractor = new AddToCartInteractorImpl(mExecutor,
                                                        mMainThread,
                                                        this,
                                                        new CartRepositoryImpl(),
                                                        device_id,
                                                        api_key,
                                                        user_id,
                                                        product_id,
                                                        size_id, quantity);
        cartInteractor.execute();
    }

    @Override
    public void onAddToCartSuccessful(AddToCartResponse response) {
        mView.hideProgressBar();
        Toast.makeText(mContext, "Successfully added to cart", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onAddToCartFail(String errorMsg) {
        mView.hideProgressBar();
        if(errorMsg.contains("GoToCart")){
            mView.showGoToCartSnackbar();
        }else{
            Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }
}
