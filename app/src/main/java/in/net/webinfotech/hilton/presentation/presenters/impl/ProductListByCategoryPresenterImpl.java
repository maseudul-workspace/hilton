package in.net.webinfotech.hilton.presentation.presenters.impl;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import in.net.webinfotech.hilton.domain.executor.Executor;
import in.net.webinfotech.hilton.domain.executor.MainThread;
import in.net.webinfotech.hilton.domain.interactor.GetProductsByCategoryInteractor;
import in.net.webinfotech.hilton.domain.interactor.impl.GetProductsByCategoryInteractorImpl;
import in.net.webinfotech.hilton.domain.model.product.Product;
import in.net.webinfotech.hilton.presentation.presenters.ProductListByCategoryPresenter;
import in.net.webinfotech.hilton.presentation.presenters.base.AbstractPresenter;
import in.net.webinfotech.hilton.presentation.routers.ProductListByCategoryRouter;
import in.net.webinfotech.hilton.presentation.ui.adapters.RecyclerViewProductList;
import in.net.webinfotech.hilton.repository.products.ProductRepositoryImpl;

/**
 * Created by Raj on 22-01-2019.
 */

public class ProductListByCategoryPresenterImpl extends AbstractPresenter implements ProductListByCategoryPresenter,
        GetProductsByCategoryInteractor.Callback, RecyclerViewProductList.Callback{

    Context mConetxt;
    ProductListByCategoryPresenter.View mView;
    GetProductsByCategoryInteractorImpl mInteractor;
    RecyclerViewProductList adapter;
    ProductListByCategoryRouter mRouter;
    Product[] newProducts;


    public ProductListByCategoryPresenterImpl(Executor executor,
                                              MainThread mainThread,
                                              Context context,
                                              ProductListByCategoryPresenter.View view,
                                              ProductListByCategoryRouter router) {
        super(executor, mainThread);
        this.mConetxt = context;
        this.mView = view;
        this.mRouter = router;
    }

    @Override
    public void getProductList(int category_id, int page_no, String device_id, String type) {
        if(type.equals("refresh")){
            newProducts = null;
        }
        mInteractor = new GetProductsByCategoryInteractorImpl(mExecutor, mMainThread, this,
                                                                new ProductRepositoryImpl(),category_id, page_no, device_id);
        mInteractor.execute();
    }

    @Override
    public void onGettingProductListSuccess(Product[] products, int totalPage) {
        Product[] tempProducts;
        tempProducts = newProducts;
        try {
            int len1 = tempProducts.length;
            int len2 = products.length;
            newProducts = new Product[len1 + len2];
            System.arraycopy(tempProducts, 0, newProducts, 0, len1);
            System.arraycopy(products, 0, newProducts, len1, len2);
            adapter.addItems(newProducts);
            adapter.notifyDataSetChanged();
            mView.hidePaginationProgressBar();
        }catch (NullPointerException e){
            newProducts = products;
            adapter = new RecyclerViewProductList(mConetxt, products, this);
            mView.loadRecyclerViewAdapter(adapter, totalPage);
            mView.hideProgressBar();
        }
        mView.stopRefreshing();
    }

    @Override
    public void onGettingProductListFail(String errorMsg) {
//        Toast.makeText(mConetxt, errorMsg,Toast.LENGTH_SHORT).show();
        mView.hideProgressBar();
        mView.hidePaginationProgressBar();
        mView.stopRefreshing();
    }

    @Override
    public void onProductClicked(int productId) {
        mRouter.goToProductDetails(productId);
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }
}
