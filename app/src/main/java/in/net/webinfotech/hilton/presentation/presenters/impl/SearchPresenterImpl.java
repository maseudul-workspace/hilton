package in.net.webinfotech.hilton.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import in.net.webinfotech.hilton.domain.executor.Executor;
import in.net.webinfotech.hilton.domain.executor.MainThread;
import in.net.webinfotech.hilton.domain.interactor.GetProductsByQueryInteractor;
import in.net.webinfotech.hilton.domain.interactor.impl.GetProductDetailsByIdInteractorImpl;
import in.net.webinfotech.hilton.domain.interactor.impl.GetProductsByCategoryInteractorImpl;
import in.net.webinfotech.hilton.domain.interactor.impl.GetProductsByQueryInteractorImpl;
import in.net.webinfotech.hilton.domain.model.product.Product;
import in.net.webinfotech.hilton.presentation.presenters.SearchPresenter;
import in.net.webinfotech.hilton.presentation.presenters.base.AbstractPresenter;
import in.net.webinfotech.hilton.presentation.routers.SearchRouter;
import in.net.webinfotech.hilton.presentation.ui.adapters.RecyclerViewProductList;
import in.net.webinfotech.hilton.repository.products.ProductRepositoryImpl;

/**
 * Created by Raj on 30-01-2019.
 */

public class SearchPresenterImpl extends AbstractPresenter implements SearchPresenter,
                                                                        GetProductsByQueryInteractor.Callback,
                                                                        RecyclerViewProductList.Callback{

    Context mContext;
    SearchPresenter.View mView;
    GetProductsByQueryInteractorImpl mInteractor;
    RecyclerViewProductList adapter;
    Product[] newProducts;
    String currentSearchKey;
    SearchRouter mRouter;

    public SearchPresenterImpl(Executor executor,
                               MainThread mainThread,
                               Context context,
                               SearchPresenter.View view,
                               SearchRouter router
                               ) {
        super(executor, mainThread);
        mContext = context;
        mView = view;
        this.mRouter = router;
    }

    @Override
    public void getProductList(String search_key, int page_no, String device_id, String type) {
        currentSearchKey = search_key;
        if(type.equals("refresh")){
            newProducts = null;
        }
        mInteractor = new GetProductsByQueryInteractorImpl(mExecutor, mMainThread,
                                                            this, new ProductRepositoryImpl(),
                                                            device_id, search_key, page_no);
        mInteractor.execute();
    }

    @Override
    public void onGettingProductListSuccess(Product[] products, String searchKey, int totalPage) {
        if(currentSearchKey.equals(searchKey)) {
            if(products.length == 0){
                adapter = new RecyclerViewProductList(mContext, products, this);
                mView.loadRecyclerViewAdapter(adapter, totalPage);
                mView.hideProgressBar();
                mView.stopRefreshing();
                mView.hidePaginationProgressBar();
                Toast.makeText(mContext, "No products found", Toast.LENGTH_SHORT).show();
            }else{
                    Product[] tempProducts;
                    tempProducts = newProducts;
                    try {
                        int len1 = tempProducts.length;
                        int len2 = products.length;
                        newProducts = new Product[len1 + len2];
                        System.arraycopy(tempProducts, 0, newProducts, 0, len1);
                        System.arraycopy(products, 0, newProducts, len1, len2);
                        adapter.addItems(newProducts);
                        adapter.notifyDataSetChanged();
                        mView.hidePaginationProgressBar();
                    } catch (NullPointerException e) {
                        newProducts = products;
                        adapter = new RecyclerViewProductList(mContext, products, this);
                        mView.loadRecyclerViewAdapter(adapter, totalPage);
                        mView.hideProgressBar();
                    }
                    mView.stopRefreshing();
            }
        }
    }

    @Override
    public void onGettingProductListFail(String errorMsg) {
        newProducts = null;
        mView.hideProgressBar();
        mView.hidePaginationProgressBar();
        mView.stopRefreshing();
    }

    @Override
    public void onProductClicked(int productId) {
        mRouter.goToProductDetails(productId);
    }

    @Override
    public void resume() {

    }


    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }
}
