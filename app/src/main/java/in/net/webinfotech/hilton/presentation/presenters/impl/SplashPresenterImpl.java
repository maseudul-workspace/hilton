package in.net.webinfotech.hilton.presentation.presenters.impl;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import in.net.webinfotech.hilton.AndroidApplication;
import in.net.webinfotech.hilton.domain.executor.Executor;
import in.net.webinfotech.hilton.domain.executor.MainThread;
import in.net.webinfotech.hilton.domain.interactor.RegisterDeviceIdInteractor;
import in.net.webinfotech.hilton.domain.interactor.impl.RegisterDeviceIdInteractorImpl;
import in.net.webinfotech.hilton.domain.model.registration.DeviceIdResponse;
import in.net.webinfotech.hilton.presentation.presenters.SplashPresenter;
import in.net.webinfotech.hilton.presentation.presenters.base.AbstractPresenter;
import in.net.webinfotech.hilton.presentation.routers.SplashRouter;
import in.net.webinfotech.hilton.repository.registration.RegistrationRepositoryImpl;

/**
 * Created by Raj on 21-01-2019.
 */

public class SplashPresenterImpl extends AbstractPresenter implements SplashPresenter, RegisterDeviceIdInteractor.Callback {

    Context mContext;
    RegisterDeviceIdInteractorImpl mInteractor;
    SplashRouter mRouter;
    AndroidApplication androidApplication;

    public SplashPresenterImpl(Executor executor, MainThread mainThread, Context context, SplashRouter router) {
        super(executor, mainThread);
        this.mContext = context;
        this.mRouter = router;
    }

    @Override
    public void sendDeviceId(String deviceId) {
        mInteractor = new RegisterDeviceIdInteractorImpl(mExecutor,
                                                        mMainThread,
                                                        this,
                                                        new RegistrationRepositoryImpl(),
                                                        deviceId);
        mInteractor.execute();
    }

    @Override
    public void onDeviceIdRegisterSuccess(DeviceIdResponse response) {
        this.mRouter.goToMain();
    }

    @Override
    public void onDeviceIdRegistrationFail(String errorMsg) {
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
        this.mRouter.goToMain();
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }
}
