package in.net.webinfotech.hilton.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import in.net.webinfotech.hilton.AndroidApplication;
import in.net.webinfotech.hilton.domain.executor.Executor;
import in.net.webinfotech.hilton.domain.executor.MainThread;
import in.net.webinfotech.hilton.domain.interactor.GetUserDetailsInteractor;
import in.net.webinfotech.hilton.domain.interactor.impl.GetUserDetailsInteractorImpl;
import in.net.webinfotech.hilton.domain.model.user.UserCompleteDetails;
import in.net.webinfotech.hilton.presentation.presenters.UserDetailsPresenter;
import in.net.webinfotech.hilton.presentation.presenters.base.AbstractPresenter;
import in.net.webinfotech.hilton.repository.user.UserRepositoryImpl;

/**
 * Created by Raj on 28-01-2019.
 */

public class UserDetailsPresenterImpl extends AbstractPresenter implements UserDetailsPresenter,
                                                                            GetUserDetailsInteractor.Callback{

    GetUserDetailsInteractorImpl mInteractor;
    AndroidApplication androidApplication;
    Context mContext;
    UserDetailsPresenter.View mView;

    public UserDetailsPresenterImpl(Executor executor,
                                    MainThread mainThread,
                                    Context context,
                                    UserDetailsPresenter.View view
                                    ) {
        super(executor, mainThread);
        this.mContext = context;
        this.mView = view;
    }

    @Override
    public void resume() {

    }

    @Override
    public void getUserDetails(String apiKey, int userId, String deviceId) {
        mInteractor = new GetUserDetailsInteractorImpl(mExecutor,
                                                        mMainThread,
                                                        new UserRepositoryImpl(),
                                                        this, deviceId, apiKey, userId);
        mInteractor.execute();
    }

    @Override
    public void onGettingUserDetailsSuccess(UserCompleteDetails userDetails) {
        mView.loadUserDetails(userDetails);
        mView.hideProgressBar();
    }

    @Override
    public void onGettingUserDetailsFail(String errorMsg) {
        mView.hideProgressBar();
        Toast.makeText(mContext, errorMsg,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }
}
