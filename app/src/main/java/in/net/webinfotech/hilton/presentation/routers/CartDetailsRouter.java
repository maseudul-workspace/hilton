package in.net.webinfotech.hilton.presentation.routers;

/**
 * Created by Raj on 30-01-2019.
 */

public interface CartDetailsRouter {
    void goToProductDetails(int productId);
}
