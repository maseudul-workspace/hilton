package in.net.webinfotech.hilton.presentation.routers;

/**
 * Created by Raj on 24-01-2019.
 */

public interface ProductListByCategoryRouter {
    void goToProductDetails(int productId);
}
