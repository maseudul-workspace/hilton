package in.net.webinfotech.hilton.presentation.routers;

/**
 * Created by Raj on 31-01-2019.
 */

public interface SearchRouter {
    void goToProductDetails(int productId);
}
