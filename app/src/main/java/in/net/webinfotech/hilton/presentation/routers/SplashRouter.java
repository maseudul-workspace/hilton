package in.net.webinfotech.hilton.presentation.routers;

/**
 * Created by Raj on 21-01-2019.
 */

public interface SplashRouter {
    void goToMain();
}
