package in.net.webinfotech.hilton.presentation.ui.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.provider.Settings;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.ExpandableDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.net.webinfotech.hilton.AndroidApplication;
import in.net.webinfotech.hilton.R;
import in.net.webinfotech.hilton.domain.executor.impl.ThreadExecutor;
import in.net.webinfotech.hilton.domain.model.cart.CartDetails;
import in.net.webinfotech.hilton.domain.model.user.UserInfo;
import in.net.webinfotech.hilton.presentation.presenters.BaseActivityPresenter;
import in.net.webinfotech.hilton.presentation.presenters.impl.BaseActivityPresenterImpl;
import in.net.webinfotech.hilton.threading.MainThreadImpl;

public class BaseActivity extends AppCompatActivity implements BaseActivityPresenter.View{

    @BindView(R.id.toolbar)
    android.support.v7.widget.Toolbar toolbar;
    @BindView(R.id.txt_cart_count)
    TextView txtCartCount;
    @BindView(R.id.drawerLayout)
    ConstraintLayout constraintLayout;
    AndroidApplication androidApplication;
    Drawer result;
    BaseActivityPresenterImpl mPresenter;
    UserInfo userInfo;
    @BindView(R.id.img_greetings_status)
    ImageView imgGreetingsStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    protected void inflateContent(@LayoutRes int inflateResID){
        setContentView(R.layout.activity_base);
        FrameLayout contentFramelayout = (FrameLayout) findViewById(R.id.content_frame);
        getLayoutInflater().inflate(inflateResID, contentFramelayout);
        ButterKnife.bind(this);
        intialisePresenter();
        setUpDrawer();

    }

    public void intialisePresenter(){
        mPresenter = new BaseActivityPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    protected void overridePendingTransitionExit() {
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }

    public void setUpDrawer() {
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("Home");

        // Create the AccountHeader
        AccountHeader headerResult = new AccountHeaderBuilder()
                .withActivity(this)
                .withHeaderBackground(R.drawable.logo)
                .withHeaderBackgroundScaleType(ImageView.ScaleType.FIT_CENTER)
                .build();

        //if you want to update the items at a later time it is recommended to keep it in a variable

        //create the drawer and remember the `Drawer` result object
        result = new DrawerBuilder()
                .withActivity(this)
                .withAccountHeader(headerResult)
                .withToolbar(toolbar)
                .withHasStableIds(true)
                .withDisplayBelowStatusBar(false)
                .withActionBarDrawerToggleAnimated(true)
                .addDrawerItems(
                        new PrimaryDrawerItem().withName("Home").withIdentifier(1).withIcon(R.drawable.home_menu).withSelectable(false),
                        new ExpandableDrawerItem().withName("Category").withIcon(R.drawable.category_menu).withSelectable(false)
                                .withSubItems(
                                        new SecondaryDrawerItem().withName("Veterinary").withIcon(R.drawable.veterenery_menu).withLevel(2).withIdentifier(100),
                                        new SecondaryDrawerItem().withName("Tablets & Capsule").withIcon(R.drawable.capsule_menu).withLevel(2).withIdentifier(101),
                                        new SecondaryDrawerItem().withName("Syrup").withIcon(R.drawable.syrup_menu).withLevel(2).withIdentifier(102),
                                        new SecondaryDrawerItem().withName("Ointment & Cream").withIcon(R.drawable.ointment_menu).withLevel(2).withIdentifier(103),
                                        new SecondaryDrawerItem().withName("Injection").withIcon(R.drawable.syringe_menu).withLevel(2).withIdentifier(104),
                                        new SecondaryDrawerItem().withName("Drops").withIcon(R.drawable.dropper_menu).withLevel(2).withIdentifier(105),
                                        new SecondaryDrawerItem().withName("Bandage & Cottons").withIcon(R.drawable.bandage_menu).withLevel(2).withIdentifier(106),
                                        new SecondaryDrawerItem().withName("Powder").withIcon(R.drawable.powder_menu).withLevel(2).withIdentifier(107),
                                        new SecondaryDrawerItem().withName("Soap,Shampoo & Lotion").withIcon(R.drawable.soap_menu).withLevel(2).withIdentifier(108),
                                        new SecondaryDrawerItem().withName("Test").withIcon(R.drawable.test_menu).withLevel(2).withIdentifier(109),
                                        new SecondaryDrawerItem().withName("Laboratory Products").withIcon(R.drawable.laboratory_menu).withLevel(2).withIdentifier(110)
                                ),
                        new PrimaryDrawerItem().withName("Contact Us").withIdentifier(3).withIcon(R.drawable.contact_menu).withSelectable(false),
                        new PrimaryDrawerItem().withName("About Us").withIdentifier(2).withIcon(R.drawable.about_menu).withSelectable(false)
                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        // do something with the clicked item :D
                        if (drawerItem != null) {
                            switch (((int) drawerItem.getIdentifier())) {
                                case 1:
                                    Toast.makeText(getApplicationContext(), "You pressed home", Toast.LENGTH_SHORT).show();
                                    break;
                                case 100:
                                    Intent intent = new Intent(getApplicationContext(), ProductListActivity.class);
                                    intent.putExtra("categoryId", 4);
                                    startActivity(intent);
                                    break;
                                case 101:
                                    Intent intent1 = new Intent(getApplicationContext(), ProductListActivity.class);
                                    intent1.putExtra("categoryId", 6);
                                    startActivity(intent1);
                                    break;
                                case 102:
                                    Intent intent2 = new Intent(getApplicationContext(), ProductListActivity.class);
                                    intent2.putExtra("categoryId", 7);
                                    startActivity(intent2);
                                    break;
                                case 103:
                                    Intent intent3 = new Intent(getApplicationContext(), ProductListActivity.class);
                                    intent3.putExtra("categoryId", 8);
                                    startActivity(intent3);
                                    break;
                                case 104:
                                    Intent intent4 = new Intent(getApplicationContext(), ProductListActivity.class);
                                    intent4.putExtra("categoryId", 9);
                                    startActivity(intent4);
                                    break;
                                case 105:
                                    Intent intent5 = new Intent(getApplicationContext(), ProductListActivity.class);
                                    intent5.putExtra("categoryId", 10);
                                    startActivity(intent5);
                                    break;
                                case 106:
                                    Intent intent6 = new Intent(getApplicationContext(), ProductListActivity.class);
                                    intent6.putExtra("categoryId", 11);
                                    startActivity(intent6);
                                    break;
                                case 107:
                                    Intent intent7 = new Intent(getApplicationContext(), ProductListActivity.class);
                                    intent7.putExtra("categoryId", 12);
                                    startActivity(intent7);
                                    break;
                                case 108:
                                    Intent intent8 = new Intent(getApplicationContext(), ProductListActivity.class);
                                    intent8.putExtra("categoryId", 13);
                                    startActivity(intent8);
                                    break;
                                case 109:
                                    Intent intent9 = new Intent(getApplicationContext(), ProductListActivity.class);
                                    intent9.putExtra("categoryId", 14);
                                    startActivity(intent9);
                                    break;
                                case 110:
                                    Intent intent10 = new Intent(getApplicationContext(), ProductListActivity.class);
                                    intent10.putExtra("categoryId", 15);
                                    startActivity(intent10);
                                    break;
                                case 5:
                                    Intent loginIntent = new Intent(getApplicationContext(), LoginActivity.class);
                                    startActivity(loginIntent);
                                    break;
                                case 2:
                                    Intent aboutIntent = new Intent(getApplicationContext(), AboutActivity.class);
                                    startActivity(aboutIntent);
                                    break;
                                case 3:
                                    Intent contactIntent = new Intent(getApplicationContext(), ContactActivity.class);
                                    startActivity(contactIntent);
                                    break;
                                case 6:
                                    androidApplication = (AndroidApplication) getApplicationContext();
                                    androidApplication.setUserInfo(getApplicationContext(), null);
                                    result.addItem(new PrimaryDrawerItem().withName("Log In").withIcon(R.drawable.login_icon).withIdentifier(5).withSelectable(false));
                                    result.removeItem(6);
                                    result.removeItem(8);
                                    result.removeItem(7);
                                    result.removeItem(10);
                                    Toast.makeText(getApplicationContext(), "Logout successful",Toast.LENGTH_SHORT).show();
                                    txtCartCount.setText("");
                                    break;
                                case 7:
                                    Intent userDetails = new Intent(getApplicationContext(), UserDetailsActivity.class);
                                    startActivity(userDetails);
                                    break;
                                case 8:
                                    Intent orderList = new Intent(getApplicationContext(), OrderListActivity.class);
                                    startActivity(orderList);
                                    break;
                                case 9:
                                    Intent ledgerActivity = new Intent(getApplicationContext(), LedgerActivity.class);
                                    startActivity(ledgerActivity);
                                    break;
                                case 10:
                                    Intent feedbackIntent = new Intent(getApplicationContext(), FeedbackActivity.class);
                                    startActivity(feedbackIntent);
                                    break;
                            }
                        }
                        return false;
                    }
                })
                .build();
        androidApplication = (AndroidApplication) getApplicationContext();
        if (androidApplication.getUserInfo(this) == null) {
            result.addItem(new PrimaryDrawerItem().withName("Log In").withIcon(R.drawable.login_icon).withIdentifier(5).withSelectable(false));
        } else {
            result.addItem(new PrimaryDrawerItem().withName("Feedback").withIdentifier(10).withIcon(R.drawable.feedback_menu).withSelectable(false));
            result.addItem(new PrimaryDrawerItem().withName("User Details").withIcon(R.drawable.user_detail_menu).withIdentifier(7).withSelectable(false));
            result.addItem(new PrimaryDrawerItem().withName("Your Orders").withIcon(R.drawable.order_menu).withIdentifier(8).withSelectable(false));
            result.addItem(new PrimaryDrawerItem().withName("Ledger").withIcon(R.drawable.ledger_menu).withIdentifier(9).withSelectable(false));
            result.addItem(new PrimaryDrawerItem().withName("Log Out").withIcon(R.drawable.logout_menu).withIdentifier(6).withSelectable(false));
        }
        actionBar.setHomeAsUpIndicator(R.drawable.ic_menu);
    }

    protected void overridePendingTransitionEnter() {
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }

    @OnClick(R.id.img_cart_btn) void goToCartDetails(){
        androidApplication = (AndroidApplication) getApplicationContext();
        if (androidApplication.getUserInfo(this) == null) {
            showLogInSnackbar();
        }else{
            Intent cartIntent = new Intent(getApplicationContext(), CartDetailsActivity.class);
            startActivity(cartIntent);
        }
    }

    @OnClick(R.id.img_search) void goToSearchActivity(){
        Intent searchIntent = new Intent(getApplicationContext(), SearchActivity.class);
        startActivity(searchIntent);
    }

    @OnClick(R.id.img_greetings) void goToGreetings(){
        androidApplication = (AndroidApplication) getApplicationContext();
        if (androidApplication.getUserInfo(this) == null) {
            showLogInSnackbar();
        }else{
            Intent greetingsIntent = new Intent(getApplicationContext(), GreetingsAcivity.class);
            startActivity(greetingsIntent);
        }
    }

    @Override
    public void loadGreetingCount(Boolean greeting) {
        if(greeting){
            imgGreetingsStatus.setVisibility(View.VISIBLE);
        }else {
            imgGreetingsStatus.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransitionExit();
    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        overridePendingTransitionEnter();
    }

    @Override
    protected void onResume() {
        super.onResume();
        androidApplication = (AndroidApplication) getApplicationContext();
        userInfo = androidApplication.getUserInfo(this);
        if ( userInfo != null) {
            @SuppressLint("HardwareIds") String deviceId = Settings.Secure.getString(this.getContentResolver(),
                    Settings.Secure.ANDROID_ID);
            deviceId = deviceId.replaceAll("^\"|\"$", "");
            mPresenter.getCartCount(deviceId, userInfo.api_key, userInfo.user_id);
            mPresenter.getGreetingCount(deviceId, userInfo.api_key, userInfo.user_id);
        }
    }

    @Override
    public void loadCartCount(int count) {
        if(count > 0){
            txtCartCount.setText(Integer.toString(count));
        }else {
            txtCartCount.setText("");
        }
    }

    public void showLogInSnackbar(){
        Snackbar snackbar = Snackbar.make(constraintLayout,"You must be logged in",Snackbar.LENGTH_LONG);
        snackbar.setAction("Log In", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cartIntent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(cartIntent);
            }
        });
        snackbar.show();
    }

}
