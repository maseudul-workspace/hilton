package in.net.webinfotech.hilton.presentation.ui.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.internal.BottomNavigationMenu;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.hilton.AndroidApplication;
import in.net.webinfotech.hilton.R;
import in.net.webinfotech.hilton.domain.executor.impl.ThreadExecutor;
import in.net.webinfotech.hilton.domain.model.user.UserInfo;
import in.net.webinfotech.hilton.presentation.presenters.CartDetailsPresenter;
import in.net.webinfotech.hilton.presentation.presenters.impl.CartDetailsPresenterImpl;
import in.net.webinfotech.hilton.presentation.routers.CartDetailsRouter;
import in.net.webinfotech.hilton.presentation.ui.adapters.RecyclerViewCartItemList;
import in.net.webinfotech.hilton.threading.MainThreadImpl;

public class CartDetailsActivity extends AppCompatActivity implements CartDetailsPresenter.View, CartDetailsRouter{

    @BindView(R.id.recycler_view_cart_details)
    RecyclerView recyclerView;
    @BindView(R.id.progressbar_layout_cart_details)
    RelativeLayout progressBarLayout;
    CartDetailsPresenterImpl mPresenter;
    AndroidApplication androidApplication;
    UserInfo userInfo;
    @BindView(R.id.navigation)
    BottomNavigationView bottomNavigationView;
    @BindView(R.id.cart_coordinatorLayout)
    CoordinatorLayout coordinatorLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart_details);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Your Cart");
        androidApplication = (AndroidApplication) getApplicationContext();
        userInfo = androidApplication.getUserInfo(this);
        @SuppressLint("HardwareIds") String deviceId = Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        deviceId = deviceId.replaceAll("^\"|\"$", "");
        initalisePresenter();
        showProgressBar();
        mPresenter.getCartDetails(deviceId, userInfo.api_key, userInfo.user_id);
        final String finalDeviceId = deviceId;
        bottomNavigationView.setOnNavigationItemReselectedListener(new BottomNavigationView.OnNavigationItemReselectedListener() {
            @Override
            public void onNavigationItemReselected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.action_checkout:
                        showProgressBar();
                        mPresenter.checkout(finalDeviceId, userInfo.api_key, userInfo.user_id, 1);
                        break;
                }
            }
        });
    }

    public void initalisePresenter(){
        mPresenter = new CartDetailsPresenterImpl(ThreadExecutor.getInstance(),
                                                    MainThreadImpl.getInstance(),
                                                    this, this, this);
    }

    @Override
    public void loadRecyclerViewAdapter(RecyclerViewCartItemList adapter) {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);
    }

    @Override
    public void showProgressBar() {
        progressBarLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        progressBarLayout.setVisibility(View.GONE);
    }

    @Override
    public void goToProductDetails(int productId) {
        Intent intent = new Intent(this, ProductDetailsActivity.class);
        intent.putExtra("productId",productId);
        startActivity(intent);
    }

    @Override
    public void showOrderSnackbar() {
        Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content),"Order placed successfully",Snackbar.LENGTH_LONG);
        snackbar.setAction("See Orders", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cartIntent = new Intent(getApplicationContext(), OrderListActivity.class);
                startActivity(cartIntent);
            }
        });
        snackbar.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
