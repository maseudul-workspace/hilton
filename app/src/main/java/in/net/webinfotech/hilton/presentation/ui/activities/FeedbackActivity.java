package in.net.webinfotech.hilton.presentation.ui.activities;

import android.annotation.SuppressLint;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.net.webinfotech.hilton.AndroidApplication;
import in.net.webinfotech.hilton.R;
import in.net.webinfotech.hilton.domain.executor.MainThread;
import in.net.webinfotech.hilton.domain.executor.impl.ThreadExecutor;
import in.net.webinfotech.hilton.domain.model.user.Feedback;
import in.net.webinfotech.hilton.domain.model.user.UserInfo;
import in.net.webinfotech.hilton.presentation.presenters.FeedbackPresenter;
import in.net.webinfotech.hilton.presentation.presenters.impl.FeedbackPresenterImpl;
import in.net.webinfotech.hilton.threading.MainThreadImpl;

public class FeedbackActivity extends AppCompatActivity implements FeedbackPresenter.View {

    FeedbackPresenterImpl mPresenter;
    AndroidApplication androidApplication;
    @SuppressLint("HardwareIds") String deviceId;
    UserInfo userInfo;
    @BindView(R.id.edit_text_feedback)
    EditText editTextFeedback;
    @BindView(R.id.progressbar_layout)
    RelativeLayout progressBarLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Give Your Valuable Feedback");
        intialisePresenter();
        androidApplication = (AndroidApplication) getApplicationContext();
        userInfo = androidApplication.getUserInfo(this);
        deviceId = Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        deviceId = deviceId.replaceAll("^\"|\"$", "");
    }

    public void intialisePresenter(){
        mPresenter = new FeedbackPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    @OnClick(R.id.btnFeedback) void giveFeedback(){
        if(editTextFeedback.getText().toString().isEmpty()){
            Toast.makeText(this, "Please write your feedback",Toast.LENGTH_SHORT).show();
        }else {
            showProgressBar();
            mPresenter.sendFeedback(deviceId, userInfo.api_key, userInfo.user_id, editTextFeedback.getText().toString());
        }
    }

    @Override
    public void showProgressBar() {
        progressBarLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void onFeedbackSuccess() {
        editTextFeedback.setText("");
    }

    @Override
    public void hideProgressBar() {
        progressBarLayout.setVisibility(View.GONE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
