package in.net.webinfotech.hilton.presentation.ui.activities;

import android.annotation.SuppressLint;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.hilton.AndroidApplication;
import in.net.webinfotech.hilton.R;
import in.net.webinfotech.hilton.domain.executor.impl.ThreadExecutor;
import in.net.webinfotech.hilton.domain.model.user.UserInfo;
import in.net.webinfotech.hilton.presentation.presenters.GreetingsPresenter;
import in.net.webinfotech.hilton.presentation.presenters.impl.GreetingsPresenterImpl;
import in.net.webinfotech.hilton.presentation.ui.adapters.RecyclerViewGreetings;
import in.net.webinfotech.hilton.threading.MainThreadImpl;

public class GreetingsAcivity extends AppCompatActivity implements GreetingsPresenter.View {

    @BindView(R.id.recycler_view_greetings)
    RecyclerView recyclerView;
    GreetingsPresenterImpl mPresenter;
    AndroidApplication androidApplication;
    @SuppressLint("HardwareIds") String deviceId;
    UserInfo userInfo;
    @BindView(R.id.progressbar_layout)
    RelativeLayout progressbarLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_greetings_acivity);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Give Your Valuable Feedback");
        initialisePresenter();
        androidApplication = (AndroidApplication) getApplicationContext();
        userInfo = androidApplication.getUserInfo(this);
        deviceId = Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        deviceId = deviceId.replaceAll("^\"|\"$", "");
        showProgressbar();
        mPresenter.getGreetings(deviceId, userInfo.api_key, userInfo.user_id);
    }

    public void initialisePresenter(){
        mPresenter = new GreetingsPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    @Override
    public void loadRecyclerViewGreetings(RecyclerViewGreetings adapter) {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);
    }

    @Override
    public void showProgressbar() {
        progressbarLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressbar() {
        progressbarLayout.setVisibility(View.GONE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
