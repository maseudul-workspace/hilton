package in.net.webinfotech.hilton.presentation.ui.activities;

import android.annotation.SuppressLint;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.hilton.AndroidApplication;
import in.net.webinfotech.hilton.R;
import in.net.webinfotech.hilton.domain.executor.impl.ThreadExecutor;
import in.net.webinfotech.hilton.domain.model.user.UserInfo;
import in.net.webinfotech.hilton.presentation.presenters.LedgerActivityPresenter;
import in.net.webinfotech.hilton.presentation.presenters.impl.LedgerActivityPresenterImpl;
import in.net.webinfotech.hilton.presentation.ui.adapters.RecyclerViewLedgerTransactions;
import in.net.webinfotech.hilton.threading.MainThreadImpl;

public class LedgerActivity extends AppCompatActivity implements LedgerActivityPresenter.View {

    LedgerActivityPresenterImpl mPresenter;
    AndroidApplication androidApplication;
    UserInfo userInfo;
    @BindView(R.id.txt_view_outstanding)
    TextView txtViewOutstanding;
    @BindView(R.id.recycler_view_ledger_transaction)
    RecyclerView recyclerView;
    @BindView(R.id.progressbar_layout)
    RelativeLayout progressBarLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ledger);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Ledger");
        initialisePresenter();
        androidApplication = (AndroidApplication) getApplicationContext();
        userInfo = androidApplication.getUserInfo(this);
        @SuppressLint("HardwareIds") String deviceId = Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        deviceId = deviceId.replaceAll("^\"|\"$", "");
        showProgressBar();
        mPresenter.getLedgerTransactions(userInfo.api_key, userInfo.user_id, deviceId);
    }

    public void initialisePresenter(){
        mPresenter = new LedgerActivityPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    @Override
    public void showProgressBar() {
        progressBarLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressbar() {
        progressBarLayout.setVisibility(View.GONE);
    }

    @Override
    public void loadAdapter(RecyclerViewLedgerTransactions adapter, Long outstanding) {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);
        txtViewOutstanding.setText("Rs. " + Long.toString(outstanding));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
