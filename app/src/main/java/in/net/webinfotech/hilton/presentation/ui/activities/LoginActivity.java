package in.net.webinfotech.hilton.presentation.ui.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.net.webinfotech.hilton.R;
import in.net.webinfotech.hilton.domain.executor.impl.ThreadExecutor;
import in.net.webinfotech.hilton.presentation.presenters.LoginPresenter;
import in.net.webinfotech.hilton.presentation.presenters.impl.LoginPresenterImpl;
import in.net.webinfotech.hilton.presentation.routers.LoginRouter;
import in.net.webinfotech.hilton.threading.MainThreadImpl;

public class LoginActivity extends AppCompatActivity implements LoginRouter,LoginPresenter.View{

    @BindView(R.id.edit_text_email)
    EditText editTextEmail;
    @BindView(R.id.edit_text_password)
    EditText editTextPassword;
    LoginPresenterImpl mPresenter;
    String deviceId;
    String email;
    String password;
    @BindView(R.id.progressbar_layout)
    RelativeLayout progressBarLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Log In");
        intializePresenter();
    }

    public void intializePresenter(){
        mPresenter = new LoginPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(),this, this, this);
    }

    @SuppressLint("HardwareIds")
    @OnClick(R.id.btnLogin) void onLoginButtonClicked(){
        if(editTextEmail.getText().toString().trim().isEmpty() || editTextPassword.getText().toString().trim().isEmpty()){
            Toast.makeText(this, "Fields cannot be empty",Toast.LENGTH_SHORT).show();
        }else{
            deviceId = Settings.Secure.getString(this.getContentResolver(),
                    Settings.Secure.ANDROID_ID);
            deviceId = deviceId.replaceAll("^\"|\"$", "");
            email = editTextEmail.getText().toString();
            password = editTextPassword.getText().toString();
            mPresenter.checkLogin(email, password, deviceId);
            showProgressBar();
        }
    }

    @Override
    public void goToMain() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void hideProgressBar() {
        progressBarLayout.setVisibility(View.GONE);
    }

    @Override
    public void showProgressBar() {
        progressBarLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
