package in.net.webinfotech.hilton.presentation.ui.activities;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.ViewFlipper;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import in.net.webinfotech.hilton.R;
import in.net.webinfotech.hilton.domain.model.category.CategoryList;
import in.net.webinfotech.hilton.presentation.ui.adapters.RecyclerViewCategoryList;

public class MainActivity extends BaseActivity {

    @BindView(R.id.viewflipper)
    ViewFlipper viewFlipper;
    List<CategoryList> categoryLists;
    RecyclerViewCategoryList adapter;
    @BindView(R.id.recycler_view_category)
    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        inflateContent(R.layout.activity_main);
        setFlipperImages();
        insertDataToCategoryList();
        loadCategorylist();
    }

    public void flipperImages(int image){
        ImageView imageView = new ImageView(this);
        imageView.setBackgroundResource(image);
        imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
        viewFlipper.addView(imageView);
        viewFlipper.setFlipInterval(2000);
        viewFlipper.setAutoStart(true);
        viewFlipper.setInAnimation(this, android.R.anim.slide_in_left);
        viewFlipper.setOutAnimation(this, android.R.anim.slide_out_right);
    }

    void setFlipperImages(){
        int sliderImages[] = {R.drawable.flipperimage1, R.drawable.flipperimage2, R.drawable.flipperimage3};
        for(int image: sliderImages){
            flipperImages(image);
        }
    }

    void insertDataToCategoryList(){
        categoryLists = new ArrayList<>();

        List<String> categoryNames = new ArrayList<String>();
        categoryNames.add("Veterinary");
        categoryNames.add("Tablets & Capsule");
        categoryNames.add("Dry Sup & Sup");
        categoryNames.add("Ointment & Cream");
        categoryNames.add("Injection");
        categoryNames.add("Eye, Ear & Nasal Drops");
        categoryNames.add("Cotton & Bandages");
        categoryNames.add("Powder");
        categoryNames.add("Soap, Shampoo & Lotion");
        categoryNames.add("Test");
        categoryNames.add("Laboratory Products");

        List<Integer> categoryIds = new ArrayList<Integer>();
        categoryIds.add(4);
        categoryIds.add(6);
        categoryIds.add(7);
        categoryIds.add(8);
        categoryIds.add(9);
        categoryIds.add(10);
        categoryIds.add(11);
        categoryIds.add(12);
        categoryIds.add(13);
        categoryIds.add(14);
        categoryIds.add(15);

        List<Integer> drawableIds = new ArrayList<Integer>();
        drawableIds.add(R.drawable.vetenary);
        drawableIds.add(R.drawable.tablets);
        drawableIds.add(R.drawable.syrup);
        drawableIds.add(R.drawable.ointment);
        drawableIds.add(R.drawable.injection);
        drawableIds.add(R.drawable.drop);
        drawableIds.add(R.drawable.bandage);
        drawableIds.add(R.drawable.powder);
        drawableIds.add(R.drawable.soap);
        drawableIds.add(R.drawable.test);
        drawableIds.add(R.drawable.laboratory);

        for(int i = 0; i < categoryNames.size(); i++){
            CategoryList list = new CategoryList(categoryNames.get(i), drawableIds.get(i), categoryIds.get(i));
            categoryLists.add(list);
        }
    }

    void loadCategorylist(){
        adapter = new RecyclerViewCategoryList(this, categoryLists);
        layoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);
    }


}
