package in.net.webinfotech.hilton.presentation.ui.activities;

import android.annotation.SuppressLint;
import android.provider.Settings;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.RelativeLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.hilton.AndroidApplication;
import in.net.webinfotech.hilton.R;
import in.net.webinfotech.hilton.domain.executor.impl.ThreadExecutor;
import in.net.webinfotech.hilton.domain.model.user.UserInfo;
import in.net.webinfotech.hilton.presentation.presenters.OrderListPresenter;
import in.net.webinfotech.hilton.presentation.presenters.impl.OrderListPresenterImpl;
import in.net.webinfotech.hilton.presentation.ui.adapters.RecyclerViewOrderIds;
import in.net.webinfotech.hilton.threading.MainThreadImpl;

public class OrderListActivity extends AppCompatActivity implements OrderListPresenter.View {

    @BindView(R.id.recycler_view_order_ids)
    RecyclerView recyclerView;
    @BindView(R.id.order_productList_progressbar_layout)
    RelativeLayout progressBarLayout;
    @BindView(R.id.order_pagination_progressbar_layout)
    RelativeLayout paginationProgressBarLayout;
    @BindView(R.id.order_list_swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    OrderListPresenterImpl mPresenter;
    AndroidApplication androidApplication;
    Boolean isScrolling = false;
    Integer currentItems;
    Integer totalItems;
    Integer scrollOutItems;
    int pageNo = 1;
    int totalPage = 1;
    String deviceId;
    String apiKey;
    int userId;
    LinearLayoutManager layoutManager;

    @SuppressLint("HardwareIds")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle("Your Orders");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initialisePresenter();
        androidApplication = (AndroidApplication) getApplicationContext();
        deviceId = Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        deviceId = deviceId.replaceAll("^\"|\"$", "");
        UserInfo userInfo = androidApplication.getUserInfo(this);
        apiKey = userInfo.api_key;
        userId = userInfo.user_id;
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                pageNo = 1;
                totalPage = 1;
                mPresenter.getOrderList(deviceId, apiKey, userId, pageNo, "refresh");
            }
        });
        showProgressBar();
        mPresenter.getOrderList(deviceId, apiKey, userId, pageNo, "");
    }

    public void initialisePresenter(){
        mPresenter = new OrderListPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    @Override
    public void loadOrdersAdapter(RecyclerViewOrderIds adapter, final int totalPageCount) {
        totalPage = totalPageCount;
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL)
                {
                    isScrolling= true;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                currentItems = layoutManager.getChildCount();
                totalItems  = layoutManager.getItemCount();
                scrollOutItems = layoutManager.findFirstCompletelyVisibleItemPosition();
                if(!recyclerView.canScrollVertically(1))
                {
                    if(pageNo < totalPage) {
                        isScrolling = false;
                        pageNo = pageNo + 1;
                        showPaginationProgressBar();
                        mPresenter.getOrderList(deviceId, apiKey, userId, pageNo, "");
                    }
                }
            }
        });
    }

    @Override
    public void showProgressBar() {
        progressBarLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        progressBarLayout.setVisibility(View.GONE);
    }

    @Override
    public void hidePaginationProgressBar() {
        paginationProgressBarLayout.setVisibility(View.GONE);
    }

    @Override
    public void showPaginationProgressBar() {
        paginationProgressBarLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void stopRefreshing() {
        if (swipeRefreshLayout != null && swipeRefreshLayout.isRefreshing())
        {
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void onOrderDeleteSuccess() {
        pageNo = 1;
        totalPage = 1;
        mPresenter.getOrderList(deviceId, apiKey, userId, pageNo, "refresh");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
