package in.net.webinfotech.hilton.presentation.ui.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.github.chrisbanes.photoview.PhotoView;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.hilton.R;
import in.net.webinfotech.hilton.util.GlideHelper;

public class PreviewImageActivity extends AppCompatActivity {

    @BindView(R.id.photoviewZoom)
    PhotoView photoView;
    String imageUrl = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview_image);
        ButterKnife.bind(this);
        if (savedInstanceState != null) {
            imageUrl = savedInstanceState.getString("IMAGE_URL");
        }else{
            imageUrl = getIntent().getStringExtra("IMAGE_URL");
        }
        GlideHelper.setImageView(this, photoView, imageUrl);
    }
}
