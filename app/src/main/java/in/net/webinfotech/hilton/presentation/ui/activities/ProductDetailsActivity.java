package in.net.webinfotech.hilton.presentation.ui.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.provider.Settings;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.net.webinfotech.hilton.AndroidApplication;
import in.net.webinfotech.hilton.R;
import in.net.webinfotech.hilton.domain.executor.MainThread;
import in.net.webinfotech.hilton.domain.executor.impl.ThreadExecutor;
import in.net.webinfotech.hilton.domain.model.product.Offer;
import in.net.webinfotech.hilton.domain.model.product.Product;
import in.net.webinfotech.hilton.domain.model.user.UserInfo;
import in.net.webinfotech.hilton.presentation.presenters.ProductDetailsPresenter;
import in.net.webinfotech.hilton.presentation.presenters.impl.ProductDetailsPresenterImpl;
import in.net.webinfotech.hilton.threading.MainThreadImpl;
import in.net.webinfotech.hilton.util.GlideHelper;

public class ProductDetailsActivity extends AppCompatActivity implements ProductDetailsPresenter.View {

    ProductDetailsPresenterImpl mPresenter;
    String deviceId;
    ArrayList<String> spinnerItems = new ArrayList<String>();
    AndroidApplication androidApplication;
    @BindView(R.id.img_productDetails_poster)
    ImageView imgProductPoster;
    @BindView(R.id.txt_productDetails_name)
    TextView txtProductName;
    @BindView(R.id.txt_productDetails_composition)
    TextView txtProductComposition;
    @BindView(R.id.txt_productDetails_description)
    TextView txtProductDescription;
    @BindView(R.id.spinner_productDetails)
    Spinner spinner;
    @BindView(R.id.productDetails_progressbar_layout)
    RelativeLayout progressBarLayout;
    @BindView(R.id.txt_productDetails_price)
    TextView txtProductPrice;
    @BindView(R.id.layout_offer)
    LinearLayout offerLayout;
    @BindView(R.id.txt_offer)
    TextView txtOffer;
    @BindView(R.id.productDetails_constraint_layout)
    ConstraintLayout constraintLayout;
    @BindView(R.id.txt_stock_status)
    TextView txtViewStockStatus;
    @BindView(R.id.edit_text_quantity)
    EditText editTextQty;
    int selectedIndex = 0;
    int productId;
    int sizeId;
    int quantity;
    @BindView(R.id.txt_view_quantity_enter)
    TextView txtViewEnterQuantity;
    Product product;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);
        ButterKnife.bind(this);
        intiialisePresenter();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        deviceId = Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        deviceId = deviceId.replaceAll("^\"|\"$", "");
        productId = getIntent().getIntExtra("productId",1);
        showProgressBar();
        mPresenter.getProductDetails(productId, deviceId);
    }

    void intiialisePresenter(){
        mPresenter = new ProductDetailsPresenterImpl(ThreadExecutor.getInstance(),
                                                        MainThreadImpl.getInstance(),
                                                        this, this);
    }

    @Override
    public void setProductDetails(final Product productDetails) {
        product = productDetails;
        final String baseUrl = getResources().getString(R.string.base_url);
        GlideHelper.setImageView(this, imgProductPoster, baseUrl + productDetails.image);
        imgProductPoster.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), PreviewImageActivity.class);
                intent.putExtra("IMAGE_URL", baseUrl + productDetails.image);
                startActivity(intent);
            }
        });
        productId = productDetails.product_id;
        txtProductName.setText(productDetails.product_name);
        getSupportActionBar().setTitle(productDetails.product_name);
        txtProductComposition.setText(productDetails.product_composition);
        txtProductDescription.setText(productDetails.description);

        for(int i = 0; i < productDetails.sizes.length; i++){
            spinnerItems.add("Rs. " + productDetails.sizes[i].price + "/" + productDetails.sizes[i].product_size + productDetails.sizes[i].size_name);
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                R.layout.spinner_text, spinnerItems);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                selectedIndex = i;
                if(productDetails.sizes[i].stock == 0){
                    txtViewStockStatus.setText("Out of Stock");
                    txtViewStockStatus.setTextColor(getResources().getColor(R.color.red2));
                    txtProductPrice.setText("Out of Stock");
                    editTextQty.setVisibility(View.GONE);
                    txtViewEnterQuantity.setVisibility(View.GONE);
                }else{
                    txtViewStockStatus.setText("In Stock");
                    txtViewStockStatus.setTextColor(getResources().getColor(R.color.green2));
                    txtViewEnterQuantity.setVisibility(View.VISIBLE);
                    editTextQty.setVisibility(View.VISIBLE);
                    editTextQty.setText("1", TextView.BufferType.EDITABLE);
                    double totalPrice = getPrice(Integer.parseInt(editTextQty.getText().toString()), productDetails.sizes[i].price);
                    txtProductPrice.setText("Total Price: Rs." + totalPrice);
                }
                sizeId = productDetails.sizes[i].size_id;
                if(productDetails.sizes[i].offers != null){
                    offerLayout.setVisibility(View.VISIBLE);
                    Offer[] offers = productDetails.sizes[i].offers;
                    String offerMsg = "";
                    for(int index = 0; index < offers.length; index++){
                        if(offers[index].offer_type == 1){
                            offerMsg = offerMsg +  "Buy " + offers[index].buy + " get " + offers[index].offer + " free";
                        }else{
                            offerMsg = offerMsg + "Rs." + offers[index].offer + " off on purchase of " + offers[index].buy;
                        }
                        offerMsg = offerMsg + "\n";
                    }
                    txtOffer.setText(offerMsg);
                }else{
                    offerLayout.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }

        });

        editTextQty.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(!editable.toString().isEmpty()){
                    double totalPrice = getPrice(Integer.parseInt(editable.toString()), productDetails.sizes[selectedIndex].price);
                    txtProductPrice.setText("Total Price: Rs." + totalPrice);
                }else{
                    txtProductPrice.setText("Total Price: Rs.0");
                }
            }
        });

    }

    double getPrice(int quantity, double price){
        double totalPrice = quantity * price;
        return totalPrice;
    }

    @OnClick(R.id.btn_add_to_cart) void addToCart(){
        androidApplication = (AndroidApplication) getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(this);
        if(userInfo == null){
            showLogInSnackbar();
        }else{
            if(product.sizes[selectedIndex].stock == 0){
                Toast.makeText(this, "Out of Stock", Toast.LENGTH_SHORT).show();
            }else if(editTextQty.getText().toString().trim().isEmpty() || Integer.parseInt(editTextQty.getText().toString()) == 0){
                Toast.makeText(this, "Please enter quantity", Toast.LENGTH_SHORT).show();
            }else if(Integer.parseInt(editTextQty.getText().toString()) > product.sizes[selectedIndex].stock){
                Toast.makeText(this, "Please enter quantity less than " + product.sizes[selectedIndex].stock, Toast.LENGTH_SHORT).show();
            }else{
                showProgressBar();
                quantity = Integer.parseInt(editTextQty.getText().toString());
                int userId = userInfo.user_id;
                String apikey = userInfo.api_key;
                @SuppressLint("HardwareIds") String deviceId = Settings.Secure.getString(this.getContentResolver(),
                        Settings.Secure.ANDROID_ID);
                deviceId = deviceId.replaceAll("^\"|\"$", "");
                mPresenter.addToCart(deviceId, apikey, userId, productId, product.sizes[selectedIndex].size_id, quantity);
            }
        }
    }

    @Override
    public void showProgressBar() {
        progressBarLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        progressBarLayout.setVisibility(View.GONE);
    }

    @Override
    public void showGoToCartSnackbar() {
        Snackbar snackbar = Snackbar.make(constraintLayout,"Item already exist in your cart",Snackbar.LENGTH_LONG);
        snackbar.setAction("Go to cart", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cartIntent = new Intent(getApplicationContext(), CartDetailsActivity.class);
                startActivity(cartIntent);
            }
        });
        snackbar.show();
    }

    public void showLogInSnackbar(){
        Snackbar snackbar = Snackbar.make(constraintLayout,"You must be logged in",Snackbar.LENGTH_LONG);
        snackbar.setAction("Log In", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cartIntent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(cartIntent);
            }
        });
        snackbar.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
