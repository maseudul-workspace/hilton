package in.net.webinfotech.hilton.presentation.ui.activities;

import android.content.Intent;
import android.provider.Settings;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.hilton.R;
import in.net.webinfotech.hilton.domain.executor.impl.ThreadExecutor;
import in.net.webinfotech.hilton.presentation.presenters.ProductListByCategoryPresenter;
import in.net.webinfotech.hilton.presentation.presenters.impl.ProductListByCategoryPresenterImpl;
import in.net.webinfotech.hilton.presentation.routers.ProductListByCategoryRouter;
import in.net.webinfotech.hilton.presentation.ui.adapters.RecyclerViewProductList;
import in.net.webinfotech.hilton.threading.MainThreadImpl;

public class ProductListActivity extends AppCompatActivity implements ProductListByCategoryPresenter.View,
                                                                        ProductListByCategoryRouter{

    ProductListByCategoryPresenterImpl mPresenter;
    @BindView(R.id.recycler_view_product_listByCategory)
    RecyclerView recyclerView;
    LinearLayoutManager layoutManager;
    int categoryId;
    String deviceId;
    @BindView(R.id.productList_progressbar_layout)
    RelativeLayout progressBarLayout;
    @BindView(R.id.pagination_progressbar_layout)
    RelativeLayout paginationProgressBarLayout;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    Boolean isScrolling = false;
    Integer currentItems;
    Integer totalItems;
    Integer scrollOutItems;
    int pageNo = 1;
    int totalPage = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Products");
        initialisePresenter();
        deviceId = Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        deviceId = deviceId.replaceAll("^\"|\"$", "");
        categoryId = getIntent().getIntExtra("categoryId",6);
        showProgressBar();
        mPresenter.getProductList(categoryId,pageNo, deviceId, "");
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                pageNo = 1;
                totalPage = 1;
                mPresenter.getProductList(categoryId, pageNo, deviceId, "refresh");
            }
        });
    }

    public void initialisePresenter(){
        mPresenter = new ProductListByCategoryPresenterImpl(ThreadExecutor.getInstance(),
                                                            MainThreadImpl.getInstance(),
                                                            this, this, this);
    }

    @Override
    public void loadRecyclerViewAdapter(RecyclerViewProductList adapter, final int totalPageCount) {
        totalPage = totalPageCount;
        layoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL)
                {
                    isScrolling= true;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                currentItems = layoutManager.getChildCount();
                totalItems  = layoutManager.getItemCount();
                scrollOutItems = layoutManager.findFirstCompletelyVisibleItemPosition();
                if(!recyclerView.canScrollVertically(1))
                {
                    if(pageNo < totalPage) {
                        isScrolling = false;
                        pageNo = pageNo + 1;
                        showPaginationProgressBar();
                        mPresenter.getProductList(categoryId, pageNo, deviceId, "");
                    }
                }
            }
        });
    }

    @Override
    public void showProgressBar() {
        progressBarLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        progressBarLayout.setVisibility(View.GONE);
    }

    @Override
    public void hidePaginationProgressBar() {
        paginationProgressBarLayout.setVisibility(View.GONE);
    }

    @Override
    public void showPaginationProgressBar() {
        paginationProgressBarLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void stopRefreshing() {
        if (swipeRefreshLayout != null && swipeRefreshLayout.isRefreshing())
        {
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void goToProductDetails(int productId) {
        Intent intent = new Intent(this, ProductDetailsActivity.class);
        intent.putExtra("productId",productId);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
