package in.net.webinfotech.hilton.presentation.ui.activities;

import android.content.Intent;
import android.provider.Settings;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.RelativeLayout;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.hilton.R;
import in.net.webinfotech.hilton.domain.executor.impl.ThreadExecutor;
import in.net.webinfotech.hilton.presentation.presenters.SearchPresenter;
import in.net.webinfotech.hilton.presentation.presenters.impl.SearchPresenterImpl;
import in.net.webinfotech.hilton.presentation.routers.SearchRouter;
import in.net.webinfotech.hilton.presentation.ui.adapters.RecyclerViewProductList;
import in.net.webinfotech.hilton.threading.MainThreadImpl;

public class SearchActivity extends AppCompatActivity implements SearchPresenter.View, SearchRouter {

    SearchView searchView;
    SearchPresenterImpl mPresenter;
    Boolean isScrolling = false;
    Integer currentItems;
    Integer totalItems;
    Integer scrollOutItems;
    int pageNo = 1;
    int totalPage = 1;
    String deviceId;
    String searchKey = "";
    LinearLayoutManager layoutManager;
    @BindView(R.id.recycler_view_product_listBySearch)
    RecyclerView recyclerView;
    @BindView(R.id.search_pagination_progressbar_layout)
    RelativeLayout paginationProgressLayout;
    @BindView(R.id.search_swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.searchList_progressbar_layout)
    RelativeLayout progressBarLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        ButterKnife.bind(this);
        deviceId = Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        deviceId = deviceId.replaceAll("^\"|\"$", "");
        intialisePresenter();
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if(!Objects.equals(searchKey, "")){
                    pageNo = 1;
                    totalPage = 1;
                    mPresenter.getProductList(searchKey, pageNo, deviceId, "refresh");
                }else{
                    stopRefreshing();
                }
            }
        });

    }

    public void intialisePresenter(){
        mPresenter = new SearchPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this, this);
    }

    @Override
    public boolean onCreateOptionsMenu( Menu menu) {
        getMenuInflater().inflate( R.menu.search_menu, menu);

        MenuItem myActionMenuItem = menu.findItem( R.id.action_search);
        searchView = (SearchView) myActionMenuItem.getActionView();
        myActionMenuItem.expandActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if(!query.isEmpty()){
                    pageNo = 1;
                    totalPage = 1;
                    searchKey = query;
                    showProgressBar();
                    mPresenter.getProductList(searchKey, pageNo, deviceId, "refresh");
                }
                return false;
            }
            @Override
            public boolean onQueryTextChange(String s) {
                // UserFeedback.show( "SearchOnQueryTextChanged: " + s);
                if(!s.isEmpty()){
                    pageNo = 1;
                    totalPage = 1;
                    searchKey = s;
                    showProgressBar();
                    mPresenter.getProductList(searchKey, pageNo, deviceId, "refresh");
                }
                return false;
            }
        });
        return true;
    }

    @Override
    public void loadRecyclerViewAdapter(RecyclerViewProductList adapter, final int totalPageCount) {
        this.totalPage = totalPageCount;
        layoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL)
                {
                    isScrolling= true;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                currentItems = layoutManager.getChildCount();
                totalItems  = layoutManager.getItemCount();
                scrollOutItems = layoutManager.findFirstCompletelyVisibleItemPosition();
                if(!recyclerView.canScrollVertically(1))
                {
                    isScrolling  = false;
                    if(pageNo < totalPage){
                        pageNo = pageNo + 1;
                        showPaginationProgressBar();
                        mPresenter.getProductList(searchKey, pageNo, deviceId, "");
                    }
                }
            }
        });
    }

    @Override
    public void hidePaginationProgressBar() {
        paginationProgressLayout.setVisibility(View.GONE);
    }

    @Override
    public void showPaginationProgressBar() {
        paginationProgressLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void showProgressBar() {
        progressBarLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        progressBarLayout.setVisibility(View.GONE);
    }

    @Override
    public void stopRefreshing() {
        if (swipeRefreshLayout != null && swipeRefreshLayout.isRefreshing())
        {
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void goToProductDetails(int productId) {
        Intent intent = new Intent(this, ProductDetailsActivity.class);
        intent.putExtra("productId",productId);
        startActivity(intent);
    }
}
