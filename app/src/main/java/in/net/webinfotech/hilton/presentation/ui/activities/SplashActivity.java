package in.net.webinfotech.hilton.presentation.ui.activities;

import android.content.Intent;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import in.net.webinfotech.hilton.R;
import in.net.webinfotech.hilton.domain.executor.impl.ThreadExecutor;
import in.net.webinfotech.hilton.presentation.presenters.impl.SplashPresenterImpl;
import in.net.webinfotech.hilton.presentation.routers.SplashRouter;
import in.net.webinfotech.hilton.threading.MainThreadImpl;

public class SplashActivity extends AppCompatActivity implements SplashRouter {

    SplashPresenterImpl mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        initialisePresenter();
        String deviceId = Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        deviceId = deviceId.replaceAll("^\"|\"$", "");
        mPresenter.sendDeviceId(deviceId);

    }

    public void initialisePresenter(){
        mPresenter = new SplashPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(),this, this);
    }

    @Override
    public void goToMain() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
