package in.net.webinfotech.hilton.presentation.ui.activities;

import android.annotation.SuppressLint;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.hilton.AndroidApplication;
import in.net.webinfotech.hilton.R;
import in.net.webinfotech.hilton.domain.executor.impl.ThreadExecutor;
import in.net.webinfotech.hilton.domain.model.user.UserCompleteDetails;
import in.net.webinfotech.hilton.domain.model.user.UserInfo;
import in.net.webinfotech.hilton.presentation.presenters.UserDetailsPresenter;
import in.net.webinfotech.hilton.presentation.presenters.impl.UserDetailsPresenterImpl;
import in.net.webinfotech.hilton.threading.MainThreadImpl;

public class UserDetailsActivity extends AppCompatActivity implements UserDetailsPresenter.View {

    @BindView(R.id.txt_user_name)
    TextView txtUserName;
    @BindView(R.id.txt_user_phoneNo)
    TextView txtUserPhoneNo;
    @BindView(R.id.txt_user_email)
    TextView txtUserEmail;
    @BindView(R.id.txt_user_state)
    TextView txtUserState;
    @BindView(R.id.txt_user_city_name)
    TextView txtUserCity;
    @BindView(R.id.txt_user_pin)
    TextView txtuserPinNo;
    @BindView(R.id.txt_user_address)
    TextView txtUserAddress;
    AndroidApplication androidApplication;
    UserDetailsPresenterImpl mPresenter;
    UserInfo userInfo;
    @BindView(R.id.userDetails_progressbar_layout)
    RelativeLayout progressBarlayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_details);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("User Details");
        initialisePresenter();
        androidApplication = (AndroidApplication) getApplicationContext();
        userInfo = androidApplication.getUserInfo(this);
        @SuppressLint("HardwareIds") String deviceId = Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        deviceId = deviceId.replaceAll("^\"|\"$", "");
        showProgressBar();
        mPresenter.getUserDetails(userInfo.api_key, userInfo.user_id, deviceId);
    }

    public void initialisePresenter(){
        mPresenter = new UserDetailsPresenterImpl(ThreadExecutor.getInstance(),
                                                    MainThreadImpl.getInstance(),
                                                        this, this);
    }

    @Override
    public void loadUserDetails(UserCompleteDetails userDetails) {
        txtUserName.setText(userDetails.name);
        txtUserEmail.setText(userDetails.email);
        txtUserState.setText(userDetails.state);
        txtUserCity.setText(userDetails.city);
        txtuserPinNo.setText(Long.toString(userDetails.pin));
        txtUserAddress.setText(userDetails.address);
        txtUserPhoneNo.setText(Long.toString(userDetails.mobile));
    }

    @Override
    public void showProgressBar() {
        progressBarlayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        progressBarlayout.setVisibility(View.GONE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
