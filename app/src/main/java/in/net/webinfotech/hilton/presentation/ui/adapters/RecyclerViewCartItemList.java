package in.net.webinfotech.hilton.presentation.ui.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.TextView;

import org.w3c.dom.Text;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.hilton.R;
import in.net.webinfotech.hilton.domain.model.cart.CartDetails;
import in.net.webinfotech.hilton.util.GlideHelper;

/**
 * Created by Raj on 29-01-2019.
 */

public class RecyclerViewCartItemList extends RecyclerView.Adapter<RecyclerViewCartItemList.ViewHolder> {


    public interface Callback{
        void onRemoveButtonClicked(int cartId);
        void onUpdateButtonClicked(int cartId, int quantity);
        void goToProductDetails(int productId);
    }

    Context mContext;
    CartDetails[] cartDetails;
    Callback mCallback;

    public RecyclerViewCartItemList(Context context, CartDetails[] details, Callback callback) {
        this.mContext = context;
        this.cartDetails = details;
        this.mCallback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_cart_details, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        String baseUrl = mContext.getResources().getString(R.string.base_url);
        GlideHelper.setImageView(mContext, holder.imgProductPoster, baseUrl + cartDetails[position].product.image);
        holder.txtProductName.setText(cartDetails[position].product.product_name);
        holder.txtSize.setText("Rs. " + cartDetails[position].product.sizes[0].price + "/" + cartDetails[position].product.sizes[0].product_size + cartDetails[position].product.sizes[0].size_name);
        holder.txtQuantitiy.setText("Quantitiy: " + cartDetails[position].quantity);
        holder.imgProductRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.onRemoveButtonClicked(cartDetails[position].cart_id);
            }
        });
//        holder.imgProductUpdate.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                mCallback.onUpdateButtonClicked(cartDetails[position].cart_id, holder.numberPicker.getValue());
//            }
//        });
        holder.txtProductName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.goToProductDetails(cartDetails[position].product.product_id);
            }
        });
        holder.imgProductPoster.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.goToProductDetails(cartDetails[position].product.product_id);
            }
        });
    }

    @Override
    public int getItemCount() {
        return cartDetails.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_cart_details_product_name)
        TextView txtProductName;
        @BindView(R.id.txt_cart_details_size)
        TextView txtSize;
        @BindView(R.id.txt_cart_details_quantity)
        TextView txtQuantitiy;
        @BindView(R.id.img_cart_details_poster)
        ImageView imgProductPoster;
        @BindView(R.id.img_cart_details_remove)
        ImageView imgProductRemove;
        @BindView(R.id.img_cart_details_update)
        ImageView imgProductUpdate;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void updateCartItem(CartDetails[] details){
        this.cartDetails = details;
    }

}
