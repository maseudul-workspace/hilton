package in.net.webinfotech.hilton.presentation.ui.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.hilton.R;
import in.net.webinfotech.hilton.domain.model.category.CategoryList;
import in.net.webinfotech.hilton.presentation.ui.activities.ProductListActivity;
import in.net.webinfotech.hilton.util.GlideHelper;

/**
 * Created by Raj on 18-01-2019.
 */

public class RecyclerViewCategoryList extends RecyclerView.Adapter<RecyclerViewCategoryList.ViewHolder> {

    Context mContext;
    List<CategoryList> categoryLists;

    public RecyclerViewCategoryList(Context mContext, List<CategoryList> categoryLists) {
        this.mContext = mContext;
        this.categoryLists = categoryLists;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_categorylist, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        GlideHelper.setImageViewWithDrawable(mContext, holder.imgViewCategoryPoster, categoryLists.get(position).drawableId);
        holder.txtViewCategoryName.setText(categoryLists.get(position).categoryName);
        holder.imgViewCategoryPoster.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, ProductListActivity.class);
                intent.putExtra("categoryId",categoryLists.get(position).categoryid);
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return categoryLists.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_view_categorylist)
        ImageView imgViewCategoryPoster;
        @BindView(R.id.txt_view_category_name)
        TextView txtViewCategoryName;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
