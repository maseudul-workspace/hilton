package in.net.webinfotech.hilton.presentation.ui.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.hilton.R;
import in.net.webinfotech.hilton.domain.model.greeting.Greeting;

/**
 * Created by Raj on 06-02-2019.
 */

public class RecyclerViewGreetings extends RecyclerView.Adapter<RecyclerViewGreetings.ViewHolder> {

    Greeting[] greetings;
    Context mContext;

    public RecyclerViewGreetings(Context context, Greeting[] greetings) {
        this.greetings = greetings;
        mContext = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_greeting, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtGreeting.setText(greetings[position].description);
        if(position%2 == 0){
            holder.txtGreeting.setBackgroundColor(mContext.getResources().getColor(R.color.colorPrimary2));
        }
    }

    @Override
    public int getItemCount() {
        return greetings.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_recycler_view_greetings)
        TextView txtGreeting;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
