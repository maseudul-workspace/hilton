package in.net.webinfotech.hilton.presentation.ui.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import butterknife.BindDimen;
import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.hilton.R;
import in.net.webinfotech.hilton.domain.model.user.LedgerTransaction;

/**
 * Created by Raj on 25-02-2019.
 */

public class RecyclerViewLedgerTransactions extends RecyclerView.Adapter<RecyclerViewLedgerTransactions.ViewHolder> {

    Context mContext;
    LedgerTransaction[] ledgerTransactions;

    public RecyclerViewLedgerTransactions(Context context, LedgerTransaction[] ledgerTransactions) {
        this.mContext = context;
        this.ledgerTransactions = ledgerTransactions;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_ledger_transactions, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewComment.setText(ledgerTransactions[position].comment);
        holder.txtViewPurchaseAmount.setText("Rs. " + Long.toString(ledgerTransactions[position].purchase_amount));
        holder.txtViewReceiveAmount.setText("Rs. " + Long.toString(ledgerTransactions[position].receive_amount));
        if(ledgerTransactions[position].status == 2){
            holder.txtViewLedgerStatus.setText("pending");
            holder.txtViewLedgerStatus.setTextColor(mContext.getResources().getColor(R.color.red1));
        }else{
            holder.txtViewLedgerStatus.setText("paid");
            holder.txtViewLedgerStatus.setTextColor(mContext.getResources().getColor(R.color.green1));
        }

        DateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        DateFormat targetFormat = new SimpleDateFormat("MMMM dd, yyyy");
        try {
            Date datePurchase = originalFormat.parse(ledgerTransactions[position].purchase_date);
            Date dateReceive = originalFormat.parse(ledgerTransactions[position].receive_date);
            String formattedDatePurchase = targetFormat.format(datePurchase);
            String formattedDateReceive = targetFormat.format(dateReceive);
            holder.txtViewPurchaseDate.setText("Date: " + formattedDatePurchase);
            holder.txtViewReceiveDate.setText("Date: " + formattedDateReceive);
        } catch (ParseException e) {
            e.printStackTrace();
            holder.txtViewPurchaseDate.setText("Date: " + ledgerTransactions[position].purchase_date);
            holder.txtViewReceiveDate.setText("Date: " + ledgerTransactions[position].receive_date);
        }

    }

    @Override
    public int getItemCount() {
        return ledgerTransactions.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_ledger_purchase_amount)
        TextView txtViewPurchaseAmount;
        @BindView(R.id.txt_ledger_purchase_date)
        TextView txtViewPurchaseDate;
        @BindView(R.id.txt_ledger_status)
        TextView txtViewLedgerStatus;
        @BindView(R.id.txt_view_ledger_receive_amount)
        TextView txtViewReceiveAmount;
        @BindView(R.id.txt_view_ledger_receive_date)
        TextView txtViewReceiveDate;
        @BindView(R.id.txt_view_ledger_comment)
        TextView txtViewComment;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
