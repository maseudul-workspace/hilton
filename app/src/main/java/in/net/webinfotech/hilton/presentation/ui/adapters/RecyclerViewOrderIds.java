package in.net.webinfotech.hilton.presentation.ui.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.hilton.R;
import in.net.webinfotech.hilton.domain.model.cart.Orders;

/**
 * Created by Raj on 31-01-2019.
 */

public class RecyclerViewOrderIds extends RecyclerView.Adapter<RecyclerViewOrderIds.ViewHolder> {

    Orders[] orders;
    Context mContext;
    RecyclerViewOrderProducts.Callback callback;

    public RecyclerViewOrderIds(Orders[] orders, Context mContext, RecyclerViewOrderProducts.Callback callback) {
        this.orders = orders;
        this.mContext = mContext;
        this.callback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_order_id, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtOrderId.setText("Order Id: " + orders[position].order_id);
        RecyclerViewOrderProducts adapter = new RecyclerViewOrderProducts(mContext, orders[position].orderDetails, callback);
        holder.recyclerViewOrderProducts.setAdapter(adapter);
        holder.recyclerViewOrderProducts.setLayoutManager(new LinearLayoutManager(mContext));
        holder.recyclerViewOrderProducts.setHasFixedSize(true);
    }

    @Override
    public int getItemCount() {
        return orders.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_order_id)
        TextView txtOrderId;
        @BindView(R.id.recycler_view_order_products)
        RecyclerView recyclerViewOrderProducts;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void addItems(Orders[] newOrders){
        orders = newOrders;
    }

}
