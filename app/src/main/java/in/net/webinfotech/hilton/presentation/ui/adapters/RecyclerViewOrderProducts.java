package in.net.webinfotech.hilton.presentation.ui.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.hilton.R;
import in.net.webinfotech.hilton.domain.model.cart.OrderDetails;
import in.net.webinfotech.hilton.util.GlideHelper;

/**
 * Created by Raj on 31-01-2019.
 */

public class RecyclerViewOrderProducts extends RecyclerView.Adapter<RecyclerViewOrderProducts.ViewHolder> {

    public interface Callback{
        void onCancelBtnClicked(int orderDetailId);
    }

    Context mContext;
    OrderDetails[] orderDetails;
    Callback mCallback;

    public RecyclerViewOrderProducts(Context context, OrderDetails[] orderDetails, Callback callback) {
        this.mContext = context;
        this.orderDetails = orderDetails;
        this.mCallback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_order_products, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        String baseUrl = mContext.getResources().getString(R.string.base_url);
        GlideHelper.setImageView(mContext, holder.imgViewPoster, baseUrl + orderDetails[position].product_image);
        holder.txtProductName.setText(orderDetails[position].product_name);
        holder.txtProductSize.setText("Size: " + orderDetails[position].product_size);
        holder.txtQuantitiy.setText("Quantity: " + orderDetails[position].order_quantity);
        switch (orderDetails[position].order_status){
            case 1:
                holder.txtProductStatus.setText("Pending");
//                holder.imgProductCancel.setVisibility(View.VISIBLE);
                break;
            case 2:
                holder.txtProductStatus.setText("Delivered");
//                holder.imgProductCancel.setVisibility(View.GONE);
                break;
            case 3:
                holder.txtProductStatus.setText("Cancelled");
//                holder.imgProductCancel.setVisibility(View.GONE);
                break;
        }

        if(orderDetails[position].offer != null){
            switch (orderDetails[position].offer_type){
                case 1:
                    holder.txtOffer.setText("Offer: " + orderDetails[position].offer + " items free");
                    break;
                case 2:
                    holder.txtOffer.setText("Offer: Rs." + orderDetails[position].offer + " off");
                    break;
            }
        }else{
            holder.txtOffer.setText("");
        }

        holder.imgProductCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.onCancelBtnClicked(orderDetails[position].order_detail_id);
            }
        });

    }

    @Override
    public int getItemCount() {
        return orderDetails.length;
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_order_poster)
        ImageView imgViewPoster;
        @BindView(R.id.txt_order_product_name)
        TextView txtProductName;
        @BindView(R.id.txt_order_product_offer)
        TextView txtOffer;
        @BindView(R.id.txt_order_product_quantity)
        TextView txtQuantitiy;
        @BindView(R.id.txt_order_product_size)
        TextView txtProductSize;
        @BindView(R.id.txt_order_product_status)
        TextView txtProductStatus;
        @BindView(R.id.img_order_product_cancel)
        ImageView imgProductCancel;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
