package in.net.webinfotech.hilton.presentation.ui.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.hilton.R;
import in.net.webinfotech.hilton.domain.model.product.Product;
import in.net.webinfotech.hilton.util.GlideHelper;

/**
 * Created by Raj on 22-01-2019.
 */

public class RecyclerViewProductList extends RecyclerView.Adapter<RecyclerViewProductList.ViewHolder> {

    public interface Callback{
        void onProductClicked(int productId);
    }

    Context mContext;
    Product[] products;

    Callback mCallback;

    public RecyclerViewProductList(Context context, Product[] products, Callback callback) {
        this.mContext = context;
        this.products = products;
        this.mCallback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_productlst, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        try {
            String baseUrl = mContext.getResources().getString(R.string.base_url);
            GlideHelper.setImageView(mContext, holder.imgViewPoster,  baseUrl + products[position].image);
            holder.txtProductName.setText(products[position].product_name);
            holder.txtProductSizes.setText("Size available: " + products[position].sizes.length);
            holder.imgViewPoster.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mCallback.onProductClicked(products[position].product_id);
                }
            });
        }catch (NullPointerException e){

        }
    }

    @Override
    public int getItemCount() {
        return products.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txtProductName)
        TextView txtProductName;
        @BindView(R.id.txtProductSizes)
        TextView txtProductSizes;
        @BindView(R.id.img_view_product_poster)
        ImageView imgViewPoster;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void addItems(Product[] products){
        this.products = products;
    }
}
