package in.net.webinfotech.hilton.repository.cart;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by Raj on 28-01-2019.
 */

public interface CartRepository {
    @POST("api/cart/add_to_cart.php")
    @FormUrlEncoded
    Call<ResponseBody> addToCart(@Field("product_id") int product_id,
                                 @Field("device_id") String device_id,
                                 @Field("api_key") String api_key,
                                 @Field("size_id") int size_id,
                                 @Field("quantity") int quantity,
                                 @Field("user_id") int user_id
                                 );

    @POST("api/cart/show_cart_items.php")
    @FormUrlEncoded
    Call<ResponseBody> getCartDetails(@Field("device_id") String device_id,
                                 @Field("api_key") String api_key,
                                 @Field("user_id") int user_id
    );

    @POST("api/cart/remove_from_cart.php")
    @FormUrlEncoded
    Call<ResponseBody> removeCartItem(@Field("device_id") String device_id,
                                      @Field("api_key") String api_key,
                                      @Field("user_id") int user_id,
                                      @Field("cart_id") int cart_id
    );

    @POST("api/cart/update_cart.php")
    @FormUrlEncoded
    Call<ResponseBody> updateCartItem(@Field("device_id") String device_id,
                                      @Field("api_key") String api_key,
                                      @Field("user_id") int user_id,
                                      @Field("cart_id") int cart_id,
                                      @Field("quantity") int quantity
    );

    @POST("api/order/order.php")
    @FormUrlEncoded
    Call<ResponseBody> checkoutCart(@Field("device_id") String device_id,
                                      @Field("api_key") String api_key,
                                      @Field("user_id") int user_id,
                                      @Field("payment_method") int payment_method
    );

    @POST("api/order/user_orders.php")
    @FormUrlEncoded
    Call<ResponseBody> getOrdersList(@Field("device_id") String device_id,
                                    @Field("api_key") String api_key,
                                    @Field("user_id") int user_id,
                                    @Field("page_no") int page_no
    );

    @POST("api/order/order_update.php")
    @FormUrlEncoded
    Call<ResponseBody> cancelOrder(@Field("device_id") String device_id,
                                    @Field("api_key") String api_key,
                                    @Field("user_id") int user_id,
                                    @Field("order_details_id") int order_details_id
    );

}
