package in.net.webinfotech.hilton.repository.cart;

import android.util.Log;

import com.google.gson.Gson;

import in.net.webinfotech.hilton.domain.model.cart.AddToCartResponse;
import in.net.webinfotech.hilton.domain.model.cart.CancelOrderResponse;
import in.net.webinfotech.hilton.domain.model.cart.CartDetailsWrapper;
import in.net.webinfotech.hilton.domain.model.cart.CheckoutCartResponse;
import in.net.webinfotech.hilton.domain.model.cart.OrdersWrapper;
import in.net.webinfotech.hilton.domain.model.cart.RemoveCartItemResponse;
import in.net.webinfotech.hilton.domain.model.cart.UpdateCartResponse;
import in.net.webinfotech.hilton.repository.APIclient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by Raj on 28-01-2019.
 */

public class CartRepositoryImpl {
    CartRepository mRepository;

    public CartRepositoryImpl() {
        mRepository = APIclient.createService(CartRepository.class);
    }

    public AddToCartResponse addToCart(String device_id, String api_key, int user_id, int product_id, int size_id, int quantity){
        AddToCartResponse cartResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> addToCart = mRepository.addToCart(product_id, device_id, api_key, size_id, quantity, user_id);

            Response<ResponseBody> response = addToCart.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    cartResponse = null;
                }else{
                    cartResponse = gson.fromJson(responseBody, AddToCartResponse.class);
                }
            } else {
                cartResponse = null;
            }
        }catch (Exception e){
            cartResponse = null;
        }
        return cartResponse;
    }

    public CartDetailsWrapper getCartDetails(String deviceID, String apiKey, int userId){
        CartDetailsWrapper detailsWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> getCartInfo = mRepository.getCartDetails(deviceID, apiKey, userId);

            Response<ResponseBody> response = getCartInfo.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    detailsWrapper = null;
                }else{
                    detailsWrapper = gson.fromJson(responseBody, CartDetailsWrapper.class);

                }
            } else {
                detailsWrapper = null;
            }
        }catch (Exception e){
            detailsWrapper = null;
        }
        return detailsWrapper;
    }

    public RemoveCartItemResponse removeCartItem(String deviceId, String apiKey, int userId, int cartId){
        RemoveCartItemResponse removeCartItemResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> removeItem = mRepository.removeCartItem(deviceId, apiKey, userId, cartId);

            Response<ResponseBody> response = removeItem.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    removeCartItemResponse = null;
                }else{
                    removeCartItemResponse = gson.fromJson(responseBody, RemoveCartItemResponse.class);
                }
            } else {
                removeCartItemResponse = null;
            }
        }catch (Exception e){
            removeCartItemResponse = null;
        }
        return removeCartItemResponse;
    }

    public UpdateCartResponse updateCartItem(String deviceId, String apiKey, int userId, int cartId, int quantity){
        UpdateCartResponse updateCartResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> updateItem = mRepository.updateCartItem(deviceId, apiKey, userId, cartId, quantity);

            Response<ResponseBody> response = updateItem.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    updateCartResponse = null;
                }else{
                    updateCartResponse = gson.fromJson(responseBody, UpdateCartResponse.class);
                }
            } else {
                updateCartResponse = null;
            }
        }catch (Exception e){
            updateCartResponse = null;
        }
        return updateCartResponse;
    }

    public CheckoutCartResponse checkoutCart(String deviceId, String apiKey, int userId, int paymentMethod){
        CheckoutCartResponse cartResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> checkout = mRepository.checkoutCart(deviceId, apiKey, userId, paymentMethod);

            Response<ResponseBody> response = checkout.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    cartResponse= null;
                }else{
                    cartResponse = gson.fromJson(responseBody, CheckoutCartResponse.class);
                }
            } else {
                cartResponse = null;
            }
        }catch (Exception e){
            cartResponse = null;
        }
        return cartResponse;
    }

    public OrdersWrapper getOrders(String deviceId, String apiKey, int userId, int pageNo){
        OrdersWrapper ordersWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> orders = mRepository.getOrdersList(deviceId, apiKey, userId, pageNo);

            Response<ResponseBody> response = orders.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    ordersWrapper = null;
                }else{
                    ordersWrapper = gson.fromJson(responseBody, OrdersWrapper.class);
                }
            } else {
                ordersWrapper = null;
            }
        }catch (Exception e){
            ordersWrapper = null;
        }
        return ordersWrapper;
    }

    public CancelOrderResponse cancelOrder(String deviceId, String apiKey, int userId, int orderDetailsId){
        CancelOrderResponse cancelOrderResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> cancel = mRepository.cancelOrder(deviceId, apiKey, userId, orderDetailsId);

            Response<ResponseBody> response = cancel.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    cancelOrderResponse = null;
                }else{
                    cancelOrderResponse = gson.fromJson(responseBody, CancelOrderResponse.class);
                }
            } else {
                cancelOrderResponse = null;
            }
        }catch (Exception e){
            cancelOrderResponse = null;
        }
        return cancelOrderResponse;
    }

}
