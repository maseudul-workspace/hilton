package in.net.webinfotech.hilton.repository.greetings;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by Raj on 06-02-2019.
 */

public interface GreetingsRepository {

    @POST("api/greetings/greeting_count.php")
    @FormUrlEncoded
    Call<ResponseBody> getGreetingsCount(@Field("api_key") String api_key,
                                         @Field("user_id") int userId,
                                         @Field("device_id") String device_id);

    @POST("api/greetings/greeting_list.php")
    @FormUrlEncoded
    Call<ResponseBody> getGreetingsList(@Field("api_key") String api_key,
                                         @Field("user_id") int userId,
                                         @Field("device_id") String device_id);

}
