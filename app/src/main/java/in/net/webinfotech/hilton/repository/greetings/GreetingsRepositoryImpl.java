package in.net.webinfotech.hilton.repository.greetings;

import android.util.Log;

import com.google.gson.Gson;

import in.net.webinfotech.hilton.domain.model.greeting.GreetingsCount;
import in.net.webinfotech.hilton.domain.model.greeting.GreetingsWrapper;
import in.net.webinfotech.hilton.repository.APIclient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by Raj on 07-02-2019.
 */

public class GreetingsRepositoryImpl {
    GreetingsRepository mRepository;

    public GreetingsRepositoryImpl() {
        mRepository = APIclient.createService(GreetingsRepository.class);
    }

    public GreetingsCount getGreetingCount(String apiKey, int userId, String deviceId){
        GreetingsCount greetingsCount;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> getCount = mRepository.getGreetingsCount(apiKey, userId, deviceId);

            Response<ResponseBody> response = getCount.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    greetingsCount = null;
                }else{
                    greetingsCount = gson.fromJson(responseBody, GreetingsCount.class);
                }
            } else {
                greetingsCount = null;
            }
        }catch (Exception e){
            greetingsCount = null;
        }
        return greetingsCount;
    }

    public GreetingsWrapper getGreetingsList(String apiKey, int userId, String deviceId){
        GreetingsWrapper greetingsWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> getGreetings = mRepository.getGreetingsList(apiKey, userId, deviceId);

            Response<ResponseBody> response = getGreetings.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    greetingsWrapper = null;
                }else{
                    greetingsWrapper = gson.fromJson(responseBody, GreetingsWrapper.class);
                }
            } else {
                greetingsWrapper = null;
            }
        }catch (Exception e){
            greetingsWrapper = null;
        }
        return  greetingsWrapper;
    }

}
