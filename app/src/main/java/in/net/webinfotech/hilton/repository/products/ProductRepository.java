package in.net.webinfotech.hilton.repository.products;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by Raj on 22-01-2019.
 */

public interface ProductRepository {
    @POST("api/product/product_list_cat_wise.php")
    @FormUrlEncoded
    Call<ResponseBody> getProductsByCategory(@Field("category_id") long category_id,
                                             @Field("page_no") long page_no,
                                             @Field("device_id") String device_id);


    @POST("api/product/product_details.php")
    @FormUrlEncoded
    Call<ResponseBody> getProductDetailsById(@Field("product_id") long product_id,
                                             @Field("device_id") String device_id);

    @POST("api/product/product_search.php")
    @FormUrlEncoded
    Call<ResponseBody> getProductsByQuery(@Field("search_key") String search_key,
                                             @Field("page_no") long page_no,
                                             @Field("device_id") String device_id);


}
