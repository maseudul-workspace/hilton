package in.net.webinfotech.hilton.repository.products;

import android.util.Log;

import com.google.gson.Gson;

import in.net.webinfotech.hilton.domain.model.product.ProductDetailsWrapper;
import in.net.webinfotech.hilton.domain.model.product.ProductListWrapper;
import in.net.webinfotech.hilton.repository.APIclient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by Raj on 22-01-2019.
 */

public class ProductRepositoryImpl {
    ProductRepository mRepository;

    public ProductRepositoryImpl() {
        mRepository = APIclient.createService(ProductRepository.class);
    }

    public ProductListWrapper getProductByCategory(int category_id, int page_no, String device_id){
        ProductListWrapper products = null;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> getProducts = mRepository.getProductsByCategory(category_id, page_no, device_id);

            Response<ResponseBody> response = getProducts.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    products = null;
                }else{
                    products = gson.fromJson(responseBody, ProductListWrapper.class);
                }
            } else {
                products = null;
            }
        }catch (Exception e){
            products = null;
        }
        return products;
    }

    public ProductDetailsWrapper getProductDetailsById(int productId, String device_id){
        ProductDetailsWrapper product = null;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> getProducts = mRepository.getProductDetailsById(productId, device_id);

            Response<ResponseBody> response = getProducts.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    product = null;
                }else{
                    product = gson.fromJson(responseBody, ProductDetailsWrapper.class);
                }
            } else {
                product = null;
            }
        }catch (Exception e){
            product = null;
        }
        return product;
    }

    public ProductListWrapper getProductsByQuery(String deviceId, String searchKey, int pageNo){
        ProductListWrapper products = null;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> getProducts = mRepository.getProductsByQuery(searchKey, pageNo, deviceId);

            Response<ResponseBody> response = getProducts.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    products = null;
                }else{
                    products = gson.fromJson(responseBody, ProductListWrapper.class);
                }
            } else {
                products = null;
            }
        }catch (Exception e){
            products = null;
        }
        return products;
    }

}
