package in.net.webinfotech.hilton.repository.registration;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

/**
 * Created by Raj on 21-01-2019.
 */

public interface RegistrationRepository {
    @Headers({
            "Content-Type: application/json",
            "Accept: application/json"
    })
    @GET("api/security/device_id_insert.php")
    Call<ResponseBody> sendDeviceId(@Query("device_id") String deviceId) ;
}
