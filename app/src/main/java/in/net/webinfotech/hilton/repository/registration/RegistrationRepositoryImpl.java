package in.net.webinfotech.hilton.repository.registration;

import android.util.Log;

import com.google.gson.Gson;

import in.net.webinfotech.hilton.domain.model.registration.DeviceIdResponse;
import in.net.webinfotech.hilton.repository.APIclient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by Raj on 21-01-2019.
 */

public class RegistrationRepositoryImpl {
    RegistrationRepository mRepository;

    public RegistrationRepositoryImpl() {
        this.mRepository = APIclient.createService(RegistrationRepository.class);
    }

    public DeviceIdResponse setDeviceId(String deviceId){
        DeviceIdResponse deviceIdResponse = null;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try{
            Call<ResponseBody> sendDeviceId = mRepository.sendDeviceId(deviceId);
            Response<ResponseBody> response = sendDeviceId.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    deviceIdResponse = null;
                }else{
                    deviceIdResponse = gson.fromJson(responseBody, DeviceIdResponse.class);
                }
            } else {
                deviceIdResponse = null;
            }
        }catch (Exception e){
            deviceIdResponse = null;
        }
        return deviceIdResponse;
    }

}
