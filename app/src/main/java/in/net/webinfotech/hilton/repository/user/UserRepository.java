package in.net.webinfotech.hilton.repository.user;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by Raj on 23-01-2019.
 */

public interface UserRepository {

    @POST("api/user/user_login_check.php")
    @FormUrlEncoded
    Call<ResponseBody> checkUserLogin(@Field("email") String email,
                                             @Field("password") String password,
                                             @Field("device_id") String device_id);

    @POST("api/user/user_profile_fetch.php")
    @FormUrlEncoded
    Call<ResponseBody> getUserDetails(@Field("api_key") String api_key,
                                      @Field("user_id") int userId,
                                      @Field("device_id") String device_id);


    @POST("api/feedback/feedback.php")
    @FormUrlEncoded
    Call<ResponseBody> giveFeedback(@Field("api_key") String api_key,
                                      @Field("user_id") int userId,
                                      @Field("device_id") String device_id,
                                        @Field("feedback") String feedback );

    @POST("api/inventory/inventory.php")
    @FormUrlEncoded
    Call<ResponseBody> getInventory(@Field("api_key") String api_key,
                                    @Field("user_id") int userId,
                                    @Field("device_id") String device_id);

}
