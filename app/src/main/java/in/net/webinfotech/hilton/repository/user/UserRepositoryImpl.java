package in.net.webinfotech.hilton.repository.user;

import android.util.Log;

import com.google.gson.Gson;

import in.net.webinfotech.hilton.domain.model.user.Feedback;
import in.net.webinfotech.hilton.domain.model.user.LedgerWrapper;
import in.net.webinfotech.hilton.domain.model.user.UserCompleteDetails;
import in.net.webinfotech.hilton.domain.model.user.UserCompleteDetailsWrapper;
import in.net.webinfotech.hilton.domain.model.user.UserInfoWrapper;
import in.net.webinfotech.hilton.repository.APIclient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by Raj on 23-01-2019.
 */

public class UserRepositoryImpl {
    UserRepository mRepository;

    public UserRepositoryImpl() {
        mRepository = APIclient.createService(UserRepository.class);
    }

    public UserInfoWrapper checkLogin(String email, String password, String deviceId){
        UserInfoWrapper infoWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> getProducts = mRepository.checkUserLogin(email, password, deviceId);

            Response<ResponseBody> response = getProducts.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    infoWrapper = null;
                }else{
                    infoWrapper = gson.fromJson(responseBody, UserInfoWrapper.class);
                }
            } else {
                infoWrapper = null;
            }
        }catch (Exception e){
            infoWrapper = null;
        }
        return infoWrapper;
    }

    public UserCompleteDetailsWrapper getUserDetails(String apiKey, int userId, String deviceId){
        UserCompleteDetailsWrapper userDetailsWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> getProducts = mRepository.getUserDetails(apiKey, userId, deviceId);

            Response<ResponseBody> response = getProducts.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    userDetailsWrapper = null;
                }else{
                    userDetailsWrapper = gson.fromJson(responseBody, UserCompleteDetailsWrapper.class);
                }
            } else {
                userDetailsWrapper = null;
            }
        }catch (Exception e){
            userDetailsWrapper = null;
        }
        return userDetailsWrapper;
    }

    public Feedback giveFeedback(String apiKey, int userId, String deviceId, String feedbackMsg){
        Feedback feedback;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> getFeedback = mRepository.giveFeedback(apiKey, userId, deviceId, feedbackMsg);

            Response<ResponseBody> response = getFeedback.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    feedback = null;
                }else{
                    feedback = gson.fromJson(responseBody, Feedback.class);
                }
            } else {
                feedback = null;
            }
        }catch (Exception e){
            feedback = null;
        }
        return feedback;
    }

    public LedgerWrapper getLedgerTransactions(String apiKey, int userId, String deviceId){
        LedgerWrapper ledgerWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> getLedger = mRepository.getInventory(apiKey, userId, deviceId);

            Response<ResponseBody> response = getLedger.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    ledgerWrapper = null;
                }else{
                    ledgerWrapper = gson.fromJson(responseBody, LedgerWrapper.class);
                }
            } else {
                ledgerWrapper = null;
            }
        }catch (Exception e){
            ledgerWrapper = null;
        }
        return ledgerWrapper;
    }

}
